module.exports = {
  root: true,
  extends: '@react-native-community',
  rules: {
    'prettier/prettier': ['error', {endOfLine: 'auto'}],
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': ['off'],
    'react-native/no-inline-styles': 'off',
    'react-hooks/exhaustive-deps': 'off',
    'no-new': 'off',
    curly: 'off',
  },
};
