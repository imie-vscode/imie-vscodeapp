import {Alert} from 'react-native';
import Toast from 'react-native-simple-toast';

export const HandleError = (
  header: string,
  body: string,
  statusCode: number,
) => {
  Alert.alert(header, body || `Code erreur ${statusCode}`, [
    {text: 'Fermer', style: 'cancel'},
  ]);
};

export const NotifyMessage = (msg: string) => {
  Toast.show(msg, Toast.SHORT);
};
