import {Platform} from 'react-native';
import PushNotification, {Importance} from 'react-native-push-notification';

PushNotification.createChannel(
  {
    channelId: 'imievscode', // (required)
    channelName: 'IMIE VSCODE APP', // (required)
    channelDescription: 'A channel to categorise your notifications', // (optional) default: undefined.
    playSound: false, // (optional) default: true
    soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
    importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
    vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
  },
  created => {}, // (optional) callback returns whether the channel was created, false means it already existed.
);

export const configureNotification = (navigation: any) => {
  // Must be outside of any component LifeCycle (such as `componentDidMount`).
  PushNotification.configure({
    // (optional) Called when Token is generated (iOS and Android)
    onRegister: function (token) {
      // console.log('TOKEN:', token);
    },

    // (required) Called when a remote is received or opened, or local notification is opened
    onNotification: function (notification) {
      navigation.navigate(notification.data?.screen, notification.data?.params);
      // on supprime la notification
      PushNotification.cancelLocalNotifications({id: String(notification.id)});

      // (required) Called when a remote is received or opened, or local notification is opened
      // notification.finish(PushNotificationIOS.FetchResult.NoData);
    },

    // IOS ONLY (optional): default: all - Permissions to register.
    permissions: {
      alert: true,
      badge: true,
      sound: true,
    },

    // Should the initial notification be popped automatically
    // default: true
    popInitialNotification: true,

    /**
   * (optional) default: true
   * - Specified if permissions (ios) and token (android and ios) will requested or not,
   * - if not, you must call PushNotificationsHandler.requestPermissions() later
   * - if you are not using remote notification or do not have Firebase installed, use this:
       requestPermissions: Platform.OS === 'ios'
   */
    requestPermissions: Platform.OS === 'ios',
  });
};

export const showNotification = (
  title: string,
  message: string,
  obj?: object,
) => {
  PushNotification.localNotification({
    title,
    message,
    channelId: 'imievscode', // (required) channelId, if the channel doesn't exist, notification will not trigger.
    userInfo: obj,
  });
};
