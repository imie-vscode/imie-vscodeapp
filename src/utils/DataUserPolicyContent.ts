const content = `<div>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Dernière mise à jour: 14 avril 2021</font></font></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cette politique de confidentialité décrit nos politiques et procédures sur la collecte, l'utilisation et la divulgation de vos informations lorsque vous utilisez le service et vous informe de vos droits à la confidentialité et de la manière dont la loi vous protège.</font></font></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nous utilisons vos données personnelles pour fournir et améliorer le service. </font><font style="vertical-align: inherit;">En utilisant le Service, vous acceptez la collecte et l'utilisation d'informations conformément à la présente politique de confidentialité. </font><font style="vertical-align: inherit;">Cette politique de confidentialité a été créée avec l'aide du </font></font><a href="https://www.privacypolicies.com/privacy-policy-generator/" target="_blank"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">générateur de politique de confidentialité</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> .</font></font></p>
<h1><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Interprétation et définitions</font></font></h1>
<h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Interprétation</font></font></h2>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Les mots dont la lettre initiale est en majuscule ont des significations définies dans les conditions suivantes. </font><font style="vertical-align: inherit;">Les définitions suivantes ont la même signification, qu'elles apparaissent au singulier ou au pluriel.</font></font></p>
<h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Définitions</font></font></h2>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Aux fins de cette politique de confidentialité:</font></font></p>
<ul>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Compte</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> signifie un compte unique créé pour que vous puissiez accéder à notre service ou à des parties de notre service.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Affilié</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> désigne une entité qui contrôle, est contrôlée par ou est sous contrôle commun avec une partie, où «contrôle» signifie la propriété de 50% ou plus des actions, participations ou autres titres habilités à voter pour l'élection des administrateurs ou d'une autre autorité de gestion .</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Application</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> désigne le logiciel fourni par la Société que vous avez téléchargé sur tout appareil électronique, nommé ImieVSCodeApp</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Société</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> (dénommée «la Société», «Nous», «Notre» ou «Notre» dans le présent Contrat) fait référence à Imie VSCode, 70 rue Marius Aufan, Levallois.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Les cookies</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> sont de petits fichiers placés sur votre ordinateur, appareil mobile ou tout autre appareil par un site Web, contenant les détails de votre historique de navigation sur ce site Web parmi ses nombreuses utilisations.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pays se</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> réfère à: France</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Appareil</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> désigne tout appareil pouvant accéder au Service tel qu'un ordinateur, un téléphone portable ou une tablette numérique.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Les données personnelles</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> sont toutes les informations relatives à une personne identifiée ou identifiable.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Le service</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> fait référence à l'application ou au site Web ou aux deux.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Prestataire de services</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> désigne toute personne physique ou morale qui traite les données pour le compte de la Société. </font><font style="vertical-align: inherit;">Il se réfère à des sociétés tierces ou à des personnes employées par la Société pour faciliter le Service, pour fournir le Service au nom de la Société, pour exécuter des services liés au Service ou pour aider la Société à analyser la façon dont le Service est utilisé.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Le service de médias sociaux tiers</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> fait référence à tout site Web ou à tout site Web de réseau social via lequel un utilisateur peut se connecter ou créer un compte pour utiliser le service.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Les données d'utilisation font</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> référence aux données collectées automatiquement, soit générées par l'utilisation du service, soit à partir de l'infrastructure du service elle-même (par exemple, la durée d'une visite de page).</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Le site Web</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> fait référence à Imie VSCode, accessible depuis </font></font><a href="https://imievscode.fr" rel="external nofollow noopener" target="_blank"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://imievscode.fr</font></font></a></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vous</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> désigne la personne qui accède ou utilise le Service, ou la société ou toute autre entité juridique au nom de laquelle cette personne accède ou utilise le Service, selon le cas.</font></font></p>
</li>
</ul>
<h1><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Collecte et utilisation de vos données personnelles</font></font></h1>
<h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Types de données collectées</font></font></h2>
<h3><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Données personnelles</font></font></h3>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Lors de l'utilisation de notre service, nous pouvons vous demander de nous fournir certaines informations personnellement identifiables qui peuvent être utilisées pour vous contacter ou vous identifier. </font><font style="vertical-align: inherit;">Les informations personnellement identifiables peuvent inclure, mais sans s'y limiter:</font></font></p>
<ul>
<li>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Adresse e-mail</font></font></p>
</li>
<li>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Prénom et nom</font></font></p>
</li>
<li>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Numéro de téléphone</font></font></p>
</li>
<li>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Adresse, état, province, code postal, ville</font></font></p>
</li>
<li>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Données d'utilisation</font></font></p>
</li>
</ul>
<h3><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Données d'utilisation</font></font></h3>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Les données d'utilisation sont collectées automatiquement lors de l'utilisation du service.</font></font></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Les données d'utilisation peuvent inclure des informations telles que l'adresse de protocole Internet de votre appareil (par exemple, l'adresse IP), le type de navigateur, la version du navigateur, les pages de notre service que vous visitez, l'heure et la date de votre visite, le temps passé sur ces pages, l'appareil unique identifiants et autres données de diagnostic.</font></font></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Lorsque vous accédez au service par ou via un appareil mobile, nous pouvons collecter certaines informations automatiquement, y compris, mais sans s'y limiter, le type d'appareil mobile que vous utilisez, l'identifiant unique de votre appareil mobile, l'adresse IP de votre appareil mobile, votre mobile système d'exploitation, le type de navigateur Internet mobile que vous utilisez, les identifiants uniques de l'appareil et d'autres données de diagnostic.</font></font></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nous pouvons également collecter des informations que votre navigateur envoie chaque fois que vous visitez notre service ou lorsque vous accédez au service par ou via un appareil mobile.</font></font></p>
<h3><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Informations provenant de services de médias sociaux tiers</font></font></h3>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">La société vous permet de créer un compte et de vous connecter pour utiliser le service via les services de médias sociaux tiers suivants:</font></font></p>
<ul>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Google</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Facebook</font></font></li>
<li>Twitter</li>
</ul>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Si vous décidez de vous inscrire ou de nous accorder l'accès à un service de médias sociaux tiers, nous pouvons collecter des données personnelles qui sont déjà associées au compte de votre service de médias sociaux tiers, telles que votre nom, votre adresse e-mail, vos activités ou Votre liste de contacts associée à ce compte.</font></font></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vous pouvez également avoir la possibilité de partager des informations supplémentaires avec la Société via le compte de votre service de médias sociaux tiers. </font><font style="vertical-align: inherit;">Si vous choisissez de fournir ces informations et données personnelles, lors de l'inscription ou autrement, vous autorisez la société à les utiliser, les partager et les stocker d'une manière conforme à la présente politique de confidentialité.</font></font></p>
<h3><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Informations collectées lors de l'utilisation de l'application</font></font></h3>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Lors de l'utilisation de notre application, afin de fournir des fonctionnalités de notre application, nous pouvons collecter, avec votre autorisation préalable:</font></font></p>
<ul>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Images et autres informations provenant de l'appareil photo et de la photothèque de votre appareil</font></font></li>
</ul>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nous utilisons ces informations pour fournir des fonctionnalités de notre service, pour améliorer et personnaliser notre service. </font><font style="vertical-align: inherit;">Les informations peuvent être téléchargées sur les serveurs de la société et / ou le serveur d'un fournisseur de services ou elles peuvent être simplement stockées sur votre appareil.</font></font></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vous pouvez activer ou désactiver l'accès à ces informations à tout moment, via les paramètres de votre appareil.</font></font></p>
<h3><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Technologies de suivi et cookies</font></font></h3>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nous utilisons des cookies et des technologies de suivi similaires pour suivre l'activité sur notre service et stocker certaines informations. </font><font style="vertical-align: inherit;">Les technologies de suivi utilisées sont des balises, des balises et des scripts pour collecter et suivre des informations et pour améliorer et analyser notre service. </font><font style="vertical-align: inherit;">Les technologies que nous utilisons peuvent inclure:</font></font></p>
<ul>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cookies ou cookies de navigateur. </font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Un cookie est un petit fichier placé sur votre appareil. </font><font style="vertical-align: inherit;">Vous pouvez demander à votre navigateur de refuser tous les cookies ou d'indiquer quand un cookie est envoyé. </font><font style="vertical-align: inherit;">Cependant, si vous n'acceptez pas les cookies, il se peut que vous ne puissiez pas utiliser certaines parties de notre service. </font><font style="vertical-align: inherit;">Sauf si vous avez ajusté les paramètres de votre navigateur afin qu'il refuse les cookies, notre service peut utiliser des cookies.</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cookies Flash. </font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Certaines fonctionnalités de notre service peuvent utiliser des objets stockés localement (ou des cookies Flash) pour collecter et stocker des informations sur vos préférences ou votre activité sur notre service. </font><font style="vertical-align: inherit;">Les cookies Flash ne sont pas gérés par les mêmes paramètres de navigateur que ceux utilisés pour les cookies de navigateur. </font><font style="vertical-align: inherit;">Pour plus d'informations sur la manière de supprimer les cookies Flash, veuillez lire "Où puis-je modifier les paramètres de désactivation ou de suppression des objets partagés locaux?" </font><font style="vertical-align: inherit;">disponible à l' </font></font><a href="https://helpx.adobe.com/flash-player/kb/disable-local-shared-objects-flash.html#main_Where_can_I_change_the_settings_for_disabling__or_deleting_local_shared_objects_" rel="external nofollow noopener" target="_blank"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">adresse https://helpx.adobe.com/flash-player/kb/disable-local-shared-objects-flash.html#main_Where_can_I_change_the_settings_for_disabling__or_deleting_local_shared_objects_</font></font></a></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Balises Web. </font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Certaines sections de notre Service et de nos e-mails peuvent contenir de petits fichiers électroniques appelés balises Web (également appelés gifs clairs, pixels invisibles et gifs à pixel unique) qui permettent à la Société, par exemple, de compter les utilisateurs qui ont visité ces pages. ou a ouvert un e-mail et pour d'autres statistiques de site Web connexes (par exemple, enregistrer la popularité d'une certaine section et vérifier l'intégrité du système et du serveur).</font></font></li>
</ul>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Les cookies peuvent être des cookies «persistants» ou «de session». </font><font style="vertical-align: inherit;">Les cookies persistants restent sur votre ordinateur personnel ou appareil mobile lorsque vous vous déconnectez, tandis que les cookies de session sont supprimés dès que vous fermez votre navigateur Web. </font><font style="vertical-align: inherit;">En savoir plus sur les cookies: </font></font><a href="https://www.privacypolicies.com/blog/cookies/" target="_blank"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">que sont les cookies? </font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">.</font></font></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nous utilisons à la fois des cookies de session et des cookies persistants aux fins décrites ci-dessous:</font></font></p>
<ul>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cookies nécessaires / essentiels</font></font></strong></p>
<p>Type: Session Cookies</p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Administré par: nous</font></font></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Objectif: Ces cookies sont essentiels pour vous fournir les services disponibles sur le site Web et pour vous permettre d'utiliser certaines de ses fonctionnalités. </font><font style="vertical-align: inherit;">Ils aident à authentifier les utilisateurs et à empêcher l'utilisation frauduleuse des comptes d'utilisateurs. </font><font style="vertical-align: inherit;">Sans ces cookies, les services que vous avez demandés ne peuvent pas être fournis et nous n'utilisons ces cookies que pour vous fournir ces services.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Politique de cookies / Avis d'acceptation des cookies</font></font></strong></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Type: Cookies persistants</font></font></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Administré par: nous</font></font></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Finalité: Ces cookies identifient si les utilisateurs ont accepté l'utilisation de cookies sur le site Web.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cookies de fonctionnalité</font></font></strong></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Type: Cookies persistants</font></font></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Administré par: nous</font></font></p>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Objectif: ces cookies nous permettent de nous souvenir des choix que vous faites lorsque vous utilisez le site Web, tels que la mémorisation de vos informations de connexion ou de votre préférence de langue. </font><font style="vertical-align: inherit;">Le but de ces cookies est de vous offrir une expérience plus personnelle et de vous éviter d'avoir à ressaisir vos préférences chaque fois que vous utilisez le site Web.</font></font></p>
</li>
</ul>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pour plus d'informations sur les cookies que nous utilisons et vos choix concernant les cookies, veuillez visiter notre Politique de cookies ou la section Cookies de notre Politique de confidentialité.</font></font></p>
<h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Utilisation de vos données personnelles</font></font></h2>
<p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">La Société peut utiliser les données personnelles aux fins suivantes:</font></font></p>
<ul>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pour fournir et maintenir notre service</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> , y compris pour surveiller l'utilisation de notre service.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pour gérer votre compte:</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> pour gérer votre inscription en tant qu'utilisateur du service. </font><font style="vertical-align: inherit;">Les données personnelles que vous fournissez peuvent vous donner accès à différentes fonctionnalités du service qui vous sont disponibles en tant qu'utilisateur enregistré.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pour l'exécution d'un contrat:</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> le développement, la conformité et l'engagement du contrat d'achat pour les produits, articles ou services que vous avez achetés ou de tout autre contrat avec nous via le service.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pour vous contacter:</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Pour vous contacter par e-mail, appels téléphoniques, SMS ou autres formes équivalentes de communication électronique, telles que les notifications push d'une application mobile concernant les mises à jour ou les communications informatives liées aux fonctionnalités, produits ou services sous contrat, y compris les mises à jour de sécurité, lorsque cela est nécessaire ou raisonnable pour leur mise en œuvre.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pour vous fournir des</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> nouvelles, des offres spéciales et des informations générales sur d'autres biens, services et événements que nous proposons et similaires à ceux que vous avez déjà achetés ou sur lesquels vous vous êtes renseigné, sauf si vous avez choisi de ne pas recevoir ces informations.</font></font></p>
</li>
<li>
<p><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pour gérer vos demandes:</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> pour assister et gérer vos demandes à nous.</font></font></p>
</li>
<li>
<p><strong>For business transfers:</strong> We may use Your information to evaluate or conduct a merger, divestiture, restructuring, reorganization, dissolution, or other sale or transfer of some or all of Our assets, whether as a going concern or as part of bankruptcy, liquidation, or similar proceeding, in which Personal Data held by Us about our Service users is among the assets transferred.</p>
</li>
<li>
<p><strong>For other purposes</strong>: We may use Your information for other purposes, such as data analysis, identifying usage trends, determining the effectiveness of our promotional campaigns and to evaluate and improve our Service, products, services, marketing and your experience.</p>
</li>
</ul>
<p>We may share Your personal information in the following situations:</p>
<ul>
<li><strong>With Service Providers:</strong> We may share Your personal information with Service Providers to monitor and analyze the use of our Service, to contact You.</li>
<li><strong>For business transfers:</strong> We may share or transfer Your personal information in connection with, or during negotiations of, any merger, sale of Company assets, financing, or acquisition of all or a portion of Our business to another company.</li>
<li><strong>With Affiliates:</strong> We may share Your information with Our affiliates, in which case we will require those affiliates to honor this Privacy Policy. Affiliates include Our parent company and any other subsidiaries, joint venture partners or other companies that We control or that are under common control with Us.</li>
<li><strong>With business partners:</strong> We may share Your information with Our business partners to offer You certain products, services or promotions.</li>
<li><strong>With other users:</strong> when You share personal information or otherwise interact in the public areas with other users, such information may be viewed by all users and may be publicly distributed outside. If You interact with other users or register through a Third-Party Social Media Service, Your contacts on the Third-Party Social Media Service may see Your name, profile, pictures and description of Your activity. Similarly, other users will be able to view descriptions of Your activity, communicate with You and view Your profile.</li>
<li><strong>With Your consent</strong>: We may disclose Your personal information for any other purpose with Your consent.</li>
</ul>
<h2>Retention of Your Personal Data</h2>
<p>The Company will retain Your Personal Data only for as long as is necessary for the purposes set out in this Privacy Policy. We will retain and use Your Personal Data to the extent necessary to comply with our legal obligations (for example, if we are required to retain your data to comply with applicable laws), resolve disputes, and enforce our legal agreements and policies.</p>
<p>The Company will also retain Usage Data for internal analysis purposes. Usage Data is generally retained for a shorter period of time, except when this data is used to strengthen the security or to improve the functionality of Our Service, or We are legally obligated to retain this data for longer time periods.</p>
<h2>Transfer of Your Personal Data</h2>
<p>Your information, including Personal Data, is processed at the Company's operating offices and in any other places where the parties involved in the processing are located. It means that this information may be transferred to — and maintained on — computers located outside of Your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from Your jurisdiction.</p>
<p>Your consent to this Privacy Policy followed by Your submission of such information represents Your agreement to that transfer.</p>
<p>The Company will take all steps reasonably necessary to ensure that Your data is treated securely and in accordance with this Privacy Policy and no transfer of Your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of Your data and other personal information.</p>
<h2>Disclosure of Your Personal Data</h2>
<h3>Business Transactions</h3>
<p>If the Company is involved in a merger, acquisition or asset sale, Your Personal Data may be transferred. We will provide notice before Your Personal Data is transferred and becomes subject to a different Privacy Policy.</p>
<h3>Law enforcement</h3>
<p>Under certain circumstances, the Company may be required to disclose Your Personal Data if required to do so by law or in response to valid requests by public authorities (e.g. a court or a government agency).</p>
<h3>Other legal requirements</h3>
<p>The Company may disclose Your Personal Data in the good faith belief that such action is necessary to:</p>
<ul>
<li>Comply with a legal obligation</li>
<li>Protect and defend the rights or property of the Company</li>
<li>Prevent or investigate possible wrongdoing in connection with the Service</li>
<li>Protect the personal safety of Users of the Service or the public</li>
<li>Protect against legal liability</li>
</ul>
<h2>Security of Your Personal Data</h2>
<p>The security of Your Personal Data is important to Us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While We strive to use commercially acceptable means to protect Your Personal Data, We cannot guarantee its absolute security.</p>
<h1>Children's Privacy</h1>
<p>Our Service does not address anyone under the age of 13. We do not knowingly collect personally identifiable information from anyone under the age of 13. If You are a parent or guardian and You are aware that Your child has provided Us with Personal Data, please contact Us. If We become aware that We have collected Personal Data from anyone under the age of 13 without verification of parental consent, We take steps to remove that information from Our servers.</p>
<p>If We need to rely on consent as a legal basis for processing Your information and Your country requires consent from a parent, We may require Your parent's consent before We collect and use that information.</p>
<h1>Links to Other Websites</h1>
<p>Our Service may contain links to other websites that are not operated by Us. If You click on a third party link, You will be directed to that third party's site. We strongly advise You to review the Privacy Policy of every site You visit.</p>
<p>We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>
<h1>Changes to this Privacy Policy</h1>
<p>We may update Our Privacy Policy from time to time. We will notify You of any changes by posting the new Privacy Policy on this page.</p>
<p>We will let You know via email and/or a prominent notice on Our Service, prior to the change becoming effective and update the "Last updated" date at the top of this Privacy Policy.</p>
<p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>
<h1>Contact Us</h1>
<p>If you have any questions about this Privacy Policy, You can contact us:</p>
<ul>
<li>
<p>By email: imievscodegroupe3@gmail.com</p>
</li>
<li>
<p>By visiting this page on our website: <a href="https://imievscode.fr/contact" rel="external nofollow noopener" target="_blank">https://imievscode.fr/contact</a></p>
</li>
</ul>
</div>`;

export default content;
