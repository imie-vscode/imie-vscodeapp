export const SEXES = [
  {
    label: 'Homme',
    value: 'Homme',
  },
  {label: 'Femme', value: 'Femme'},
];

export const TEMPLATES = ['REACT', 'ANGULAR', 'VUE'];

export const COLORS_BY_LANGUAGES = {
  HTML: '#EA2027',
  JavaScript: '#FFC312',
  CSS: '#D980FA',
  SQL: '#2f3640',
  PHP: '#7f8fa6',
  TypeScript: '#3742fa',
  JSON: '#2ed573',
  XML: '#ff6348',
  Autres: '#b33939',
};

export type ColorsByLanguagesType =
  | 'HTML'
  | 'JavaScript'
  | 'CSS'
  | 'SQL'
  | 'PHP'
  | 'TypeScript'
  | 'JSON'
  | 'XML'
  | 'Autres';
