import React from 'react';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  BillStackScreen,
  HomeStackScreen,
  ProfileStackScreen,
  ProjectStackScreen,
} from './StackScreen';

const Tab = createMaterialBottomTabNavigator();

const TabNavigator = () => {
  return (
    <Tab.Navigator
      labeled={false}
      sceneAnimationEnabled
      shifting
      barStyle={{backgroundColor: '#302d42'}}>
      <Tab.Screen
        name="Accueil"
        component={HomeStackScreen}
        options={{
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="home" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Profil"
        component={ProfileStackScreen}
        options={{
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="account" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Mes projets"
        component={ProjectStackScreen}
        options={{
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="code-tags" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Mes factures"
        component={BillStackScreen}
        options={{
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name="currency-eur"
              color={color}
              size={26}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};
export default TabNavigator;
