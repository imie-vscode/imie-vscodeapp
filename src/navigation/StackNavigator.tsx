// * https://reactnavigation.org/docs/auth-flow/
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import TabNavigator from './TabNavigator';
import {useCookies} from 'react-cookie';
import {RootStackScreen} from './StackScreen';
import {SocketContext, socket} from '../context/socket';

const StackNavigator = (props: any) => {
  const [cookie] = useCookies(['user']);

  // si l'utilisateur n'est pas connecté alors on affiche la page de connexion
  // sinon on affiche l'interface mobile
  return (
    <SocketContext.Provider value={socket}>
      <NavigationContainer>
        {cookie?.user?.token ? <TabNavigator /> : <RootStackScreen />}
      </NavigationContainer>
    </SocketContext.Provider>
  );
};

export default StackNavigator;
