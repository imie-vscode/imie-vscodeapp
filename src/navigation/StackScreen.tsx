import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {Image, StyleSheet, TouchableOpacity} from 'react-native';
import HomeScreen from '../screens/HomeScreen';
import LoginScreen from '../screens/LoginScreen';
import ProfileScreen from '../screens/ProfileScreen';
import EditProfileScreen from '../screens/EditProfileScreen';
import DataUsePolicyScreen from '../screens/DataUsePolicyScreen';
import AboutScreen from '../screens/AboutScreen';
import CgvScreen from '../screens/CgvScreen';
import CguScreen from '../screens/CguScreen';
import SubscriptionBillScreen from '../screens/SubscriptionBillScreen';
import SubscriptionsScreen from '../screens/SubscriptionsScreen';
import SubscriptionPaymentScreen from '../screens/SubscriptionPaymentScreen';
import ForgotPasswordScreen from '../screens/ForgotPasswordScreen';
import ComplementaryFormScreen from '../screens/ComplementaryFormScreen';
import ListProjectsScreen from '../screens/ListProjectsScreen';
import ProjectScreen from '../screens/ProjectScreen';

// logo en haut au milieu de l'écran
export const LogoTitle = ({navigation}: {navigation: any}) => {
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('Accueil', {screen: 'Accueil'})}>
      <Image
        style={styles.logo}
        source={require('../../images/IMIECode.png')}
      />
    </TouchableOpacity>
  );
};

// Pages de connexion avec sa déclinaison inscription si l'utilisateur n'a pas de compte
const RootStack = createStackNavigator();

export const RootStackScreen = () => {
  return (
    <RootStack.Navigator>
      <RootStack.Screen
        name="Connexion"
        options={{title: 'Connexion'}}
        component={LoginScreen}
      />
      <RootStack.Screen
        name="Mot de passe oublié"
        options={{title: 'Mot de passe oublié'}}
        component={ForgotPasswordScreen}
      />
      <RootStack.Screen
        name="Formulaire complémentaire"
        options={{title: 'Complétez vos informations'}}
        component={ComplementaryFormScreen}
      />
    </RootStack.Navigator>
  );
};

// Page d'accueil
const HomeStack = createStackNavigator();

export const HomeStackScreen = () => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="Accueil"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={HomeScreen}
      />
    </HomeStack.Navigator>
  );
};

// Page profil
const ProfileStack = createStackNavigator();

export const ProfileStackScreen = () => {
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen
        name="Profil"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={ProfileScreen}
      />
      <ProfileStack.Screen
        name="Modifier mon profil"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={EditProfileScreen}
      />
      {/* ############################
              PARTIE ABONNEMENT
       ############################  */}
      <ProfileStack.Screen
        name="Abonnement - factures"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={SubscriptionBillScreen}
      />
      <ProfileStack.Screen
        name="Abonnements"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={SubscriptionsScreen}
      />
      <ProfileStack.Screen
        name="Abonnement paiement"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={SubscriptionPaymentScreen}
      />
      {/* ############################
              PARTIE A PROPOS
       ############################# */}
      <ProfileStack.Screen
        name="A propos"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={AboutScreen}
      />
      <ProfileStack.Screen
        name="Politique d'utilisation des données"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={DataUsePolicyScreen}
      />
      <ProfileStack.Screen
        name="Conditions générales d'utilisation"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={CguScreen}
      />
      <ProfileStack.Screen
        name="Conditions générales de vente"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={CgvScreen}
      />
    </ProfileStack.Navigator>
  );
};

// Page des projets
const ProjectStack = createStackNavigator();

export const ProjectStackScreen = () => {
  return (
    <ProjectStack.Navigator>
      <ProjectStack.Screen
        name="Mes projets"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={ListProjectsScreen}
      />
      <ProjectStack.Screen
        name="Projet"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={ProjectScreen}
      />
    </ProjectStack.Navigator>
  );
};

// Page des factures
const BillStack = createStackNavigator();

export const BillStackScreen = () => {
  return (
    <BillStack.Navigator>
      <BillStack.Screen
        name="Abonnement - factures"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={SubscriptionBillScreen}
      />
      <BillStack.Screen
        name="Abonnements"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={SubscriptionsScreen}
      />
      <BillStack.Screen
        name="Abonnement paiement"
        options={({navigation}) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={SubscriptionPaymentScreen}
      />
    </BillStack.Navigator>
  );
};

const styles = StyleSheet.create({
  logo: {
    marginTop: 10,
    width: 55,
    height: 55,
  },
});
