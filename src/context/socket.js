import React from 'react';
import io from 'socket.io-client';
import {SOCKET_BASE_URL} from '../../config';

export const socket = io(SOCKET_BASE_URL, {
  secure: true,
  withCredentials: true,
  transports: ['websocket'],
});
export const SocketContext = React.createContext();
