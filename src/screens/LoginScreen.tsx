/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {
  Image,
  Linking,
  Pressable,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Input, SocialIcon} from 'react-native-elements';
import axios from 'axios';
import {API_BASE_URL} from '../../config';
import {isExisted, isEmail} from '../utils/Validators';
import {HandleError} from '../utils/HandleMessage';
import {useCookies} from 'react-cookie';
import {AccessToken, LoginManager} from 'react-native-fbsdk';
import {authorize} from 'react-native-app-auth';
import {
  GoogleSignin,
  statusCodes,
  User,
} from '@react-native-google-signin/google-signin';
import {ScrollView} from 'react-native-gesture-handler';
import Config from 'react-native-config';

// base config
const GITHUB_CONFIG = {
  redirectUrl: 'com.imievscodeapp://',
  clientId: Config.GITHUB_CLIENT_ID,
  clientSecret: Config.GITHUB_CLIENT_SECRET,
  scopes: ['identity', 'user:email'],
  additionalHeaders: {Accept: 'application/json'},
  serviceConfiguration: {
    authorizationEndpoint: 'https://github.com/login/oauth/authorize',
    tokenEndpoint: 'https://github.com/login/oauth/access_token',
    revocationEndpoint:
      'https://github.com/settings/connections/applications/6127dfa44327d44bd168',
  },
};

const LoginScreen = ({navigation}: {navigation: any}) => {
  const [, setCookie] = useCookies(['user', 'facebook', 'google', 'github']);
  const [state, setState] = useState({
    email: '',
    password: '',
  });
  const [disabled, setDisabled] = useState(true);

  useEffect(() => {
    let error = 0;
    if (!isEmail(state.email)) {
      error++;
    }
    if (!isExisted(state.password)) {
      error++;
    }

    error === 0 ? setDisabled(false) : setDisabled(true);
  }, [state]);

  useEffect(() => {
    GoogleSignin.configure({
      scopes: ['profile', 'email'],
      webClientId: Config.GOOGLE_CLIENT_ID, // client ID of type WEB for your server (needed to verify user ID and offline access)
    });
  }, []);

  const onSubmit = async () => {
    try {
      const {data} = await axios.post(`${API_BASE_URL}/auth/login`, state);
      setCookie('user', data);
    } catch (error: any) {
      console.log('error: ', error);
      HandleError(
        'Erreur',
        error.response?.data.error || 'Connexion impossible',
        error.response?.status || 500,
      );
    }
  };

  const facebookConnection = async (fbData: any) => {
    let userInfo = null;
    try {
      // on récupère les infos du profil facebook
      const {data: fbProfile} = await axios.get(
        'https://graph.facebook.com/v10.0/me?fields=id%2Cname%2Cbirthday%2Clocation%2Chometown%2Cgender%2Cfirst_name%2Cemail%2Clast_name',
        {
          params: {
            access_token: fbData.accessToken,
          },
        },
      );
      userInfo = JSON.parse(JSON.stringify(fbProfile));
      console.log('fbProfile: ', userInfo);
      // on teste la connexion avec Facebook
      const {data} = await axios.post(`${API_BASE_URL}/auth/login/facebook`, {
        facebookId: userInfo.id,
        email: userInfo.email,
      });
      setCookie('user', data);
      // on met à jour la valeur comme quoi Facebook est connecté pour le déconnecter à la main si l'utilisateur se déconnecte
      setCookie('facebook', '1');
    } catch (error: any) {
      console.log('error: ', error);
      if (error.response.data.error === 'Utilisateur introuvable') {
        const birthday = userInfo.birthday.split('/');
        const user = {
          birthday: {
            day: birthday[1],
            month: birthday[0],
            year: birthday[2],
          },
          email: userInfo.email,
          firstname: userInfo.first_name,
          lastname: userInfo.last_name,
          city: userInfo.location.name,
          id: userInfo.id,
        };
        return navigation.navigate('Formulaire complémentaire', {
          user,
          service: 'FACEBOOK',
        });
      }
      // on déconnecte l'utilisateur de facebook
      // LoginManager.logOut();
      HandleError(
        'Erreur',
        error.response?.data.error || 'Connexion impossible',
        error.response?.status || 500,
      );
    }
  };

  const googleConnection = async () => {
    let userInfo = null;
    try {
      await GoogleSignin.hasPlayServices();
      userInfo = await GoogleSignin.signIn();
      // on vérifie si l'utilisateur est inscrit avec Google sur notre plateforme
      const {data} = await axios.post(`${API_BASE_URL}/auth/login/google`, {
        googleId: userInfo.user.id,
        email: userInfo.user.email,
      });
      // on met à jour ses cookies avec le token et ses infos pour qu'il soit redirigé vers son tableau de bord
      setCookie('user', data);
      // on met à jour la valeur comme quoi Google est connecté pour le déconnecter à la main si l'utilisateur se déconnecte
      setCookie('google', '1');
    } catch (error: any) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        return;
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
        return;
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        HandleError('Erreur', 'Service non disponible', 500);
        return;
      } else {
        // si le message d'erreur est "Utilisateur introuvable", alors on le redirige vers la page d'inscription avec ses infos Google
        if (error.response.data.error === 'Utilisateur introuvable') {
          const {user} = userInfo as User;
          navigation.navigate('Formulaire complémentaire', {
            user,
            service: 'GOOGLE',
          });
        }
      }
    }
  };

  const githubConnection = async () => {
    let userInfo = null;
    try {
      const authState = await authorize(GITHUB_CONFIG);

      // on récupère les infos du profil github
      const {data: githubData} = await axios.get(
        'https://api.github.com/user',
        {
          headers: {
            Accept: 'application/vnd.github.v3+json',
            Authorization: `token ${authState.accessToken}`,
          },
        },
      );
      // si le courriel est nul celà veut dire que l'utilisateur n'a pas activé le partage de son courriel
      if (githubData.email === null) {
        HandleError(
          'Erreur',
          "Vous n'avez pas activé le partage de votre courriel dans les paramètres Github de votre compte. Veuillez vous connecter manuellement",
          500,
        );
        return;
      }

      // formatage des données
      userInfo = {
        email: githubData.email,
        id: String(githubData.id),
        pseudo: githubData.login,
        firstname: githubData.name,
      };

      // on vérifie si l'utilisateur est inscrit avec Github sur notre plateforme
      const {data} = await axios.post(`${API_BASE_URL}/auth/login/github`, {
        githubId: userInfo.id,
        email: userInfo.email,
      });
      // on met à jour ses cookies avec le token et ses infos pour qu'il soit redirigé vers son tableau de bord
      setCookie('user', data);
      // on met à jour la valeur comme quoi Google est connecté pour le déconnecter à la main si l'utilisateur se déconnecte
      setCookie('github', '1');
    } catch (error: any) {
      // si le message d'erreur est "Utilisateur introuvable", alors on le redirige vers la page d'inscription avec ses infos Github
      if (error.response.data.error === 'Utilisateur introuvable') {
        navigation.navigate('Formulaire complémentaire', {
          user: userInfo,
          service: 'GITHUB',
        });
      } else {
        console.log('error: ', error);
        HandleError(
          'Erreur',
          error.response?.data.error || 'Connexion impossible',
          error.response?.status || 500,
        );
      }
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={{width: '100%', flex: 1}}>
        <Image
          style={styles.image}
          source={require('../../images/logo-inverse-color.png')}
        />
        <View style={{alignItems: 'center', width: '100%'}}>
          <View style={{width: '75%'}}>
            <Text style={styles.label}>Courriel</Text>
            <Input
              style={styles.input}
              placeholder="courriel@gmail.com"
              inputStyle={{color: 'white'}}
              textContentType="emailAddress"
              keyboardType="email-address"
              value={state.email}
              onChangeText={value =>
                setState(prev => ({...prev, email: value}))
              }
              leftIcon={{
                type: 'font-awesome',
                name: 'envelope',
                color: 'white',
                size: 17,
              }}
            />
          </View>
          <View style={{width: '75%'}}>
            <Text style={styles.label}>Mot de passe</Text>
            <Input
              placeholder="***************"
              style={styles.input}
              textContentType="password"
              secureTextEntry
              onChangeText={value =>
                setState(prev => ({...prev, password: value}))
              }
              value={state.password}
              inputStyle={{color: 'white'}}
              leftIcon={{
                type: 'font-awesome',
                name: 'lock',
                color: 'white',
                size: 17,
              }}
            />
          </View>

          <View style={styles.linkBlock}>
            <Text
              style={styles.linkBlockText}
              onPress={() => navigation.navigate('Mot de passe oublié')}>
              Mot de passe oublié
            </Text>
            <Text
              style={styles.linkBlockText}
              onPress={() =>
                Linking.openURL(Config.SITE_URL).catch(() =>
                  HandleError(
                    'Erreur',
                    "Erreur lors de l'ouverture du site web",
                    500,
                  ),
                )
              }>
              S'inscrire
            </Text>
          </View>
          <Pressable
            android_ripple={{color: '#F8AE6B'}}
            disabled={disabled}
            onPress={onSubmit}
            style={[styles.button, {opacity: disabled ? 0.5 : 1}]}>
            <Text style={styles.buttonText}>Se connecter</Text>
          </Pressable>

          <View
            style={{
              backgroundColor: 'white',
              height: 1,
              width: '75%',
              marginTop: 15,
              marginBottom: 7.5,
            }}
          />
          <View style={styles.socials}>
            <SocialIcon
              raised
              iconColor="white"
              iconType="font-awesome"
              onPress={async () => {
                try {
                  const result = await LoginManager.logInWithPermissions([
                    'user_birthday',
                    'user_hometown',
                    'user_location',
                    'user_gender',
                    'user_age_range',
                    'email',
                    'public_profile',
                  ]);
                  if (!result.isCancelled) {
                    const data: any = await AccessToken.getCurrentAccessToken();

                    await facebookConnection(data);
                  }
                } catch (error) {
                  console.log('error: ', error);
                  HandleError(
                    'Erreur',
                    'Erreur lors de la connexion avec Facebook',
                    500,
                  );
                }
              }}
              Component={TouchableOpacity}
              style={{width: '100%', borderRadius: 5}}
              fontStyle={{fontSize: 13}}
              iconSize={18}
              title="Se connecter avec Facebook"
              button
              type="facebook"
            />
            <SocialIcon
              raised
              iconColor="white"
              iconType="font-awesome"
              onPress={githubConnection}
              Component={TouchableOpacity}
              style={{width: '100%', borderRadius: 5}}
              fontStyle={{fontSize: 13}}
              iconSize={18}
              title="Se connecter avec Github"
              button
              type="github"
            />
            <SocialIcon
              raised
              iconColor="white"
              iconType="font-awesome"
              onPress={googleConnection}
              Component={TouchableOpacity}
              style={{width: '100%', borderRadius: 5}}
              fontStyle={{fontSize: 13}}
              iconSize={18}
              title="Se connecter avec Google"
              button
              type="google"
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#302d42',
    alignItems: 'center',
  },
  input: {
    borderWidth: 0,
    fontSize: 15,
  },
  label: {
    color: 'white',
    fontSize: 14,
  },
  button: {
    marginTop: 20,
    borderColor: '#F8AE6B',
    width: '75%',
    borderWidth: 2,
    borderRadius: 5,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
  },

  buttonText: {
    color: '#F8AE6B',
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
  },
  linkBlock: {
    marginTop: -15,
    paddingRight: 5,
    paddingLeft: 5,
    flexDirection: 'row',
    width: '75%',
    justifyContent: 'space-between',
  },
  linkBlockText: {
    color: 'white',
    fontSize: 12,
    textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
  },
  image: {
    width: 125,
    height: 125,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  socials: {
    flex: 1,
    flexDirection: 'column',
    width: '75%',
    alignItems: 'center',
  },
});

export default LoginScreen;
