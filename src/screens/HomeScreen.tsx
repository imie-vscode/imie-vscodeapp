/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import axios from 'axios';
import {useCookies} from 'react-cookie';
import {API_BASE_URL} from '../../config';
import {HandleError} from '../utils/HandleMessage';
import {useIsFocused} from '@react-navigation/native';
import {LineChart, PieChart} from 'react-native-chart-kit';
import moment from 'moment';
import 'moment/locale/fr';
import {ColorsByLanguagesType, COLORS_BY_LANGUAGES} from '../utils/Constantes';
moment.locale('fr');
import LottieView from 'lottie-react-native';
import {configureNotification} from '../utils/Notification';

const screenWidth = Dimensions.get('window').width;

const chartConfig = {
  backgroundColor: '#302d42',
  backgroundGradientFrom: '#302d42',
  backgroundGradientTo: '#302d42',
  decimalPlaces: 2, // optional, defaults to 2dp
  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
  labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
  style: {
    borderRadius: 16,
  },
  propsForDots: {
    r: '3',
    strokeWidth: '2',
    stroke: '#F8AE6B',
  },
};

interface IData {
  labels: string[];
  datasets: Array<{
    data: number[];
    color: () => string;
    strokeWidth: number;
  }>;
  legend: string[];
}

interface IPieData {
  name: string;
  lang: number;
  color: string;
  legendFontColor: string;
  legendFontSize: number;
}

const HomeScreen = (props: any) => {
  const [cookie] = useCookies(['user']);
  const headers = {authorization: `Bearer ${cookie.user.token}`};
  const isFocused = useIsFocused();
  const [waiting, setWaiting] = useState(true);
  const [state, setState] = useState<IData[]>([]);
  const [pieLang, setPieLan] = useState<IPieData[]>([]);

  // on initilaise le système de notification
  configureNotification(props.navigation);

  useEffect(() => {
    if (isFocused) {
      fetchStats();
    }
  }, [isFocused]);

  const fetchStats = async () => {
    try {
      const {
        data,
      } = await axios.get(
        `${API_BASE_URL}/user/${cookie.user.user._id}/stats`,
        {headers},
      );

      // #######################################
      // on ajoute le nombre de caractères
      // #######################################
      const nbCaracteres = {
        labels: data.sessions.map((stat: any) =>
          moment(stat.createdAt).format('L'),
        ),
        datasets: [
          {
            data:
              data.sessions.length > 0
                ? data.sessions.map((stat: any) => Number(stat.chars))
                : [0],
            color: () => '#F8AE6B',
            strokeWidth: 2,
          },
        ],
        legend: ['Nombres de caractères'],
      };

      // #######################################
      // On ajoute le nombre de fichiers créé
      // #######################################
      const nbFichiers = {
        labels: data.sessions.map((stat: any) =>
          moment(stat.createdAt).format('L'),
        ),
        datasets: [
          {
            data:
              data.sessions.length > 0
                ? data.sessions.map((stat: any) => Number(stat.files))
                : [0],
            color: () => '#F8AE6B',
            strokeWidth: 2,
          },
        ],
        legend: ['Nombres de fichiers créé'],
      };

      // #######################################
      // On ajoute le nombre de dossiers créé
      // #######################################
      const nbDossiers = {
        labels: data.sessions.map((stat: any) =>
          moment(stat.createdAt).format('L'),
        ),
        datasets: [
          {
            data:
              data.sessions.length > 0
                ? data.sessions.map((stat: any) => Number(stat.folders))
                : [0],
            color: () => '#F8AE6B',
            strokeWidth: 2,
          },
        ],
        legend: ['Nombres de dossiers créé'],
      };

      // #######################################
      // On ajoute le pourcentage des langages utilisés dans les projets
      // #######################################
      // on déclare une variable pour stocker tous les langages de tous les projets
      const lang = {} as {[key: string]: number};
      // incrémenter le nombre de fichiers total pour le pourcentage
      let nombresFichiers = 0;

      for (const project of data.projects) {
        nombresFichiers += project.numberOfFiles as number;
        Object.entries(project.languages).forEach(entry => {
          const [key, value] = entry as [string, number];
          // si le langage existe on incrémente sinon on l'initialise
          lang[key] = lang[key] ? lang[key] + value : value;
        });
      }

      // calcul du pourcentage
      const pourcentage = {} as {[key: string]: number};
      Object.entries(lang).forEach(entry => {
        const [key, value] = entry;
        pourcentage[key] = Number(((value / nombresFichiers) * 100).toFixed(2));
      });

      const pieData = [] as IPieData[];
      Object.entries(pourcentage).forEach(entry => {
        const [key, value] = entry as [ColorsByLanguagesType, number];
        pieData.push({
          name: key,
          lang: value,
          color: COLORS_BY_LANGUAGES[key],
          legendFontColor: COLORS_BY_LANGUAGES[key],
          legendFontSize: 12,
        });
      });

      setState([nbCaracteres, nbFichiers, nbDossiers]);
      setPieLan(pieData);

      setWaiting(false);
    } catch (error: any) {
      console.log('error: ', error);
      HandleError(
        'Erreur',
        error.response?.data.error ||
          'Erreur lors de la récupération des statistiques',
        error.response?.status || 500,
      );
    }
  };

  if (waiting) {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <LottieView source={require('../loader.json')} autoPlay loop />
      </SafeAreaView>
    );
  }
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scrollView}>
        {state.length > 0 &&
          state.map((stats, i) => (
            <LineChart
              key={i.toString()}
              data={stats}
              width={screenWidth - 30}
              style={styles.card}
              height={220}
              chartConfig={chartConfig}
              bezier
            />
          ))}

        {pieLang.length > 0 && (
          <View style={styles.cardPie}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 18,
                marginTop: 15,
                color: '#302d42',
              }}>
              Langages utilisés
            </Text>

            <PieChart
              hasLegend
              data={pieLang}
              width={screenWidth}
              height={220}
              chartConfig={chartConfig}
              accessor="lang"
              backgroundColor="transparent"
              paddingLeft="15"
              absolute
            />
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  scrollView: {
    width: '100%',
    flex: 1,
    padding: 15,
    marginBottom: 15,
  },
  card: {
    flex: 1,
    marginTop: 10,
    marginBottom: 25,
    alignSelf: 'center',
    width: '100%',
    borderRadius: 15,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.5,
    shadowRadius: 15,
  },
  cardPie: {
    flex: 1,
    marginTop: 10,
    marginBottom: 25,
    alignSelf: 'center',
    alignItems: 'center',
    width: '100%',
    borderRadius: 15,
    borderColor: 'silver',
    borderWidth: 0.5,
    elevation: 2,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    backgroundColor: 'white',
  },
});

export default HomeScreen;
