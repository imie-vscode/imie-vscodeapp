/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {
  Modal,
  Pressable,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {Card, Icon, ListItem} from 'react-native-elements';
import {useCookies} from 'react-cookie';
import {NotifyMessage, HandleError} from '../utils/HandleMessage';
import {ScrollView} from 'react-native-gesture-handler';
import {LinesLoader} from 'react-native-indicator';
import {Picker} from '@react-native-picker/picker';
import axios from 'axios';
import moment from 'moment';
import {API_BASE_URL} from '../../config';
moment.locale('fr');

const SubscriptionBillScreen = ({navigation}: any) => {
  const [cookie, setCookie] = useCookies(['user']);
  const headers = {authorization: `Bearer ${cookie.user.token}`};
  const [modalVisible, setModalVisible] = useState(false);
  const [bills, setBills] = useState([]);
  const [idBill, setIdBill] = useState('');
  const [state, setState] = useState({
    price: '',
    subscribedSince: '',
    name: '',
    advantages: [],
  });
  const [resetWaiting, setResetWaiting] = useState(false);
  const [billWaiting, setBillWaiting] = useState(false);

  // mise à jour des valeurs de l'abonnement par le cookie utilisateur
  useEffect(() => {
    setState({
      price: cookie.user?.user.subscription.price || '',
      // si la date d'abonnement n'existe pas on met la date de création du compte
      subscribedSince:
        cookie.user?.user.subscription.subscribedSince ||
        cookie.user?.user.createdAt ||
        '',
      name: cookie.user?.user.subscription.name || '',
      advantages: cookie.user?.user.subscription.advantages || [],
    });
  }, [cookie]);

  // on récupère les factures
  useEffect(() => {
    fetchFactures();
  }, []);

  const fetchFactures = async () => {
    try {
      const {data} = await axios.get(
        `${API_BASE_URL}/user/${cookie.user.user._id}/bill`,
        {
          headers,
        },
      );

      setBills(
        data.map((bill: any) => {
          return {
            label: `Facture du ${moment(bill.createdAt).format('LLL')} (${
              bill.idUser.subscription.price
            }€)`,
            key: bill._id,
          };
        }),
      );
    } catch (error) {
      console.log('error: ', error);
      HandleError(
        'Erreur',
        error.response?.data.error ||
          'Erreur lors de la récupération des factures',
        error.response?.status || 500,
      );
    }
  };

  const removeSubscription = async () => {
    setResetWaiting(true);
    try {
      const {data} = await axios.delete(
        `${API_BASE_URL}/user/${cookie.user.user._id}/subscription`,
        {
          headers,
        },
      );

      // on met à jour le cookie avec les nouvelles infos
      // on veille à bien garder le token
      const userCookie = cookie.user;
      setCookie('user', {...userCookie, user: data.user});
      NotifyMessage(data.message);
    } catch (error) {
      console.log('error: ', error);
      HandleError(
        'Erreur',
        error.response?.data.error ||
          "Erreur lors de la résiliation de l'abonnement",
        error.response?.status || 500,
      );
    }
    setResetWaiting(false);
    setModalVisible(false);
  };

  const modalResetSubscription = () => (
    <Modal
      transparent
      animationType="slide"
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(!modalVisible);
      }}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>
            Voulez-vous vraiment résilier votre abonnement ?
          </Text>
          <Text style={styles.modalSubText}>
            Si oui, votre abonnement passera à l'offre "Gratuit"
          </Text>
          {resetWaiting ? (
            <View
              style={{
                marginTop: 25,
              }}>
              <LinesLoader color="#CA48F6" />
            </View>
          ) : (
            <View
              style={{
                flexDirection: 'row',
                marginTop: 25,
              }}>
              <Pressable
                android_ripple={{color: 'white'}}
                style={[styles.button, styles.buttonConfirm]}
                onPress={removeSubscription}>
                <Text style={styles.textStyle}>Oui</Text>
              </Pressable>
              <Pressable
                android_ripple={{color: 'white'}}
                style={[styles.button, styles.buttonClose]}
                onPress={() => setModalVisible(false)}>
                <Text style={styles.textStyle}>Annuler</Text>
              </Pressable>
            </View>
          )}
        </View>
      </View>
    </Modal>
  );

  const getBill = async () => {
    setBillWaiting(true);
    try {
      const {data} = await axios.get(
        `${API_BASE_URL}/user/${cookie.user.user._id}/bill/${idBill}/pdf`,
        {
          headers,
        },
      );
      NotifyMessage(data.message);
    } catch (error) {
      console.log('error: ', error);
      HandleError(
        'Erreur',
        error.response?.data.error ||
          'Erreur lors de la récupération de la facture',
        error.response?.status || 500,
      );
    }
    setBillWaiting(false);
  };

  return (
    <SafeAreaView
      style={[
        styles.container,
        {
          opacity: modalVisible ? 0.2 : 1,
        },
      ]}>
      <Text
        style={{
          fontSize: 22,
          fontWeight: 'bold',
          textAlign: 'center',
          color: '#302d42',
          marginTop: 20,
          marginBottom: 20,
        }}>
        Abonnement - Factures
      </Text>
      <ScrollView style={{width: '100%'}}>
        {/* Modal de la résiliation d'un abonnement */}
        {modalResetSubscription()}

        <Card containerStyle={styles.cardContainer}>
          {/* Titre de l'abonnement  */}
          <View style={styles.cardTitleContainer}>
            <Card.Title style={styles.cardTitle}>{state.name}</Card.Title>
            <Card.Title style={styles.cardPrice}>
              {state.price + ' € /mois'}
            </Card.Title>
          </View>
          <Card.Divider style={{backgroundColor: 'white'}} />

          {/* Avantages de l'abonnement  */}
          {state.advantages.map((advantage: string, i: number) => (
            <ListItem
              key={i.toString()}
              containerStyle={styles.cardAdvantagesContainer}>
              <Icon name="star" size={10} color="#f1c40f" type="font-awesome" />
              <ListItem.Content>
                <ListItem.Title style={{color: 'white'}}>
                  {advantage}
                </ListItem.Title>
              </ListItem.Content>
            </ListItem>
          ))}

          {/* Message de la durée de l'abonnement  */}
          <View style={{marginTop: 15}}>
            <Text style={styles.cardSubscribedSince}>
              {state.subscribedSince !== '' &&
                'Abonnement activé depuis le ' +
                  moment(state.subscribedSince).format('LL')}
            </Text>
          </View>

          {/* Bouttons  */}
          <View style={styles.buttonsGroupContainer}>
            <Pressable
              android_ripple={{color: 'white'}}
              style={styles.buttonEditSubscription}
              onPress={() => navigation.navigate('Abonnements')}>
              <Text style={styles.buttonText}>Modifier</Text>
            </Pressable>
            <Pressable
              android_ripple={{color: 'white'}}
              style={styles.buttonRemoveSubscription}
              onPress={() => setModalVisible(true)}>
              <Text style={styles.buttonText}>Résilier</Text>
            </Pressable>
          </View>
        </Card>

        <Card
          containerStyle={[
            styles.cardContainer,
            {borderWidth: 2, backgroundColor: 'white', borderColor: '#302d42'},
          ]}>
          <Card.Title style={[styles.cardTitle, {color: '#302d42'}]}>
            Factures
          </Card.Title>
          <Card.Divider style={{backgroundColor: '#302d42'}} />
          <View>
            {/* <Select
              width={350}
              data={bills}
              placeholder="Sélectionner une facture"
              onSelect={(key: string) => setIdBill(key)}
            /> */}
            <Picker
              selectedValue={idBill}
              onValueChange={(itemValue, itemIndex) => setIdBill(itemValue)}>
              {bills.map((bill: any, i: number) => (
                <Picker.Item
                  key={i.toString()}
                  label={bill.label}
                  value={bill.key}
                />
              ))}
            </Picker>
          </View>
          {billWaiting ? (
            <View
              style={{
                marginTop: 25,
                alignItems: 'center',
              }}>
              <LinesLoader color="#CA48F6" />
            </View>
          ) : (
            <Pressable
              onPress={getBill}
              android_ripple={{color: 'white'}}
              style={styles.buttonGetBill}>
              <Text style={styles.buttonText}>Récupérer</Text>
            </Pressable>
          )}
        </Card>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  cardContainer: {
    borderWidth: 0,
    borderRadius: 5,
    backgroundColor: '#302d42',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },

  cardTitleContainer: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
  },
  cardTitle: {fontSize: 20, fontWeight: 'bold', color: 'white'},
  cardPrice: {
    backgroundColor: '#27ae60',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 7.5,
    paddingRight: 7.5,
    color: 'white',
    borderRadius: 5,
  },
  cardSubscribedSince: {
    textAlign: 'right',
    color: 'white',
    fontStyle: 'italic',
  },
  cardAdvantagesContainer: {
    backgroundColor: '#302d42',
    padding: 0,
    marginBottom: 10,
  },
  buttonsGroupContainer: {
    marginTop: 25,
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
  },
  buttonEditSubscription: {
    backgroundColor: '#F8AE6B',
    width: '60%',
    borderWidth: 0,
    borderRadius: 5,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
  },

  buttonRemoveSubscription: {
    backgroundColor: '#EA2027',
    width: '35%',
    borderWidth: 0,
    borderRadius: 5,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 17,
    textAlign: 'center',
  },
  buttonGetBill: {
    marginTop: 25,
    backgroundColor: '#F8AE6B',
    width: '100%',
    borderWidth: 0,
    borderRadius: 5,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
  },

  // MODAL
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 5,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    minWidth: 100,
    borderRadius: 5,
    padding: 10,
    elevation: 2,
  },
  buttonConfirm: {
    backgroundColor: '#CA48F6',
    marginRight: 10,
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    fontSize: 18,
    marginBottom: 5,
    textAlign: 'center',
  },
  modalSubText: {
    fontSize: 16,
    fontStyle: 'italic',
    marginBottom: 15,
    textAlign: 'justify',
  },
});

export default SubscriptionBillScreen;
