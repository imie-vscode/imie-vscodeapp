/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useContext, useEffect, useState} from 'react';
import {Pressable, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {Badge, Icon, ListItem, Overlay} from 'react-native-elements';
import {NotifyMessage, HandleError} from '../utils/HandleMessage';
import {useCookies} from 'react-cookie';
import {ScrollView} from 'react-native-gesture-handler';
import Orientation from 'react-native-orientation-locker';
import {useIsFocused} from '@react-navigation/native';
import {SocketContext} from '../context/socket';
import {WebView} from 'react-native-webview';
import {showNotification} from '../utils/Notification';

const ProjectScreen = ({route}: any) => {
  const [cookie, setCookie, removeCookie] = useCookies([
    'user',
    'sessionJoined',
    'salons',
  ]);
  // const headers = {authorization: `Bearer ${cookie.user.token}`};
  const isFocused = useIsFocused();
  const [visible, setVisible] = useState(false);
  const [participants, setParticipants] = useState({
    room: 0,
    session: 0,
  });
  const socket = useContext(SocketContext);
  const [usersProject, setUsersProject] = useState<{
    all: any | null;
    open: boolean;
  }>({
    all: null,
    open: false,
  });

  const [sessions, setSessions] = useState<{
    active: any | null;
    open: boolean;
    users: any | null;
  }>({
    active: null,
    open: false,
    users: null,
  });

  const [stream, setStream] = useState('');

  useEffect(() => {
    if (isFocused) {
      rejoindreLeSalon();
      fetchUsers();
      fetchSession();
      // si l'utilisateur a déjà rejoint la session
      if (cookie.sessionJoined) {
        rejoindreLaSession(cookie.sessionJoined);
      }
    } else {
      quitterLaSession();
    }

    return () => {
      // on remet le téléphone au format portrait par defaut
      setTimeout(() => {
        Orientation.lockToPortrait();
      }, 100);
      quitterLaSession();
    };
  }, [route.params._id, isFocused]);

  useEffect(() => {
    socket.on('ROOM_PARTICIPANTS', (data: {users: any | null}) => {
      if (data.users) {
        setUsersProject(prev => ({...prev, all: data.users}));
      }
    });

    socket.on(
      'NUMBER_SESSION_PARTICIPANTS',
      (data: {users: number; id_session: string}) => {
        setParticipants(prev => ({...prev, session: data.users}));
        recupererLesUtilisateursEnLigneDuneSession(data.id_session);
      },
    );

    socket.on(
      'SESSION_CLOSED',
      (data: {id_session: string; message: string}) => {
        Orientation.lockToPortrait();
        // on supprime la valeur du stockage locale
        removeCookie('sessionJoined');
        setVisible(false);

        // mise à jour des variables
        setSessions(prev => ({...prev, active: null, users: null}));
        setParticipants(prev => ({...prev, session: 0}));
        NotifyMessage(data.message);
      },
    );

    socket.on('NEW_SESSION_CREATED', (data: {session: any}) => {
      setSessions(prev => ({...prev, active: data.session}));
      showNotification(
        'Nouvelle session 💻',
        `Une nouvelle session sur le projet ${route.params.projectName} vient d'apparaître`,
        {screen: route.name, params: route.params},
      );
      // on envoi des WS pour récupérer les participants de la session
      socket.emit(
        'GET_NUMBER_SESSION_PARTICIPANTS',
        {id_session: data.session.uuid},
        (response: number) => {
          setParticipants(prev => ({...prev, session: response}));
        },
      );
    });

    socket.on('BROADCAST_STREAM_DATA', (str: string) => {
      setStream(str);
    });
  }, [socket]);

  const rejoindreLeSalon = () => {
    socket.emit(
      'JOIN_ROOM',
      {id_project: route.params._id, id_user: cookie.user.user._id},
      (response: 'JOINED') => {
        if (response === 'JOINED') {
          const salons = cookie.salons;

          if (salons) {
            // on vérifie si l'id n'existe pas
            if (salons.includes(route.params._id)) {
              return;
            }
            salons.push(route.params._id);
            setCookie('salons', salons);
          } else {
            setCookie('salons', [route.params._id]);
          }
        }
      },
    );
  };

  const fetchUsers = async () => {
    socket.emit(
      'GET_USERS_IN_ROOM',
      {id_project: route.params._id, id_user: cookie.user.user._id},
      (response: {code: number; users: any[] | null; message?: string}) => {
        if (response.code === 200 && response.users) {
          setUsersProject(prev => ({...prev, all: response.users as any[]}));
        } else {
          HandleError('Erreur', response.message as string, response.code);
        }
      },
    );
  };

  const fetchSession = async () => {
    socket.emit(
      'LIST_ONLINE_SESSION_IN_THE_PROJECT',
      {id_project: route.params._id, id_user: cookie.user.user._id},
      async (response: {code: number; session: any}) => {
        setSessions(prev => ({...prev, active: response.session}));

        // on envoi des WS pour récupérer les participants de la session
        socket.emit(
          'GET_NUMBER_SESSION_PARTICIPANTS',
          {id_session: response.session.uuid},
          (res: number) => {
            setParticipants(prev => ({...prev, session: res}));
          },
        );
        recupererLesUtilisateursEnLigneDuneSession(response.session.uuid);
      },
    );
  };

  const quitterLaSession = () => {
    const id_session = cookie.sessionJoined;
    // emit USER_ONLINE event
    socket.emit('LEAVE_SESSION', {
      id_project: route.params._id,
      id_user: cookie.user.user._id,
    });

    removeCookie('sessionJoined');

    // on supprime le participant de la session dans la liste sauf si on change de page
    const users = JSON.parse(JSON.stringify(sessions.users));
    if (users) {
      delete users[cookie.user.user._id];
    }
    // on met à jour les valeurs des nombres de participants
    setParticipants(prev => ({...prev, session: participants.session - 1}));
    setSessions(prev => ({...prev, users: users || null}));
  };

  const recupererLesUtilisateursEnLigneDuneSession = (id_session: string) => {
    socket.emit(
      'GET_USERS_IN_SESSION',
      {
        id_project: route.params._id,
        id_user: cookie.user.user._id,
        id_session,
      },
      (response: {code: number; users: any | null}) => {
        setSessions(prev => ({...prev, users: response.users}));
      },
    );
  };

  const rejoindreLaSession = (id_session: string) => {
    socket.emit(
      'JOIN_SESSION',
      {id_project: route.params._id, id_user: cookie.user.user._id, id_session},
      (response: {code: number; message: string}) => {
        if (response.code === 200) {
          setCookie('sessionJoined', id_session);
          NotifyMessage(response.message);

          recupererLesUtilisateursEnLigneDuneSession(id_session);
        } else {
          HandleError('Erreur', response.message, response.code);
        }
      },
    );
  };

  const ModalSession = () => (
    <Overlay
      isVisible={visible}
      onBackdropPress={() => setVisible(false)}
      fullScreen
      focusable={visible}>
      <View
        style={{
          position: 'absolute',
          zIndex: 100,
          right: 5,
          top: 5,
          backgroundColor: 'white',
          borderRadius: 50,
          padding: 5,
        }}>
        <Icon
          onPress={() => {
            quitterLaSession();
            Orientation.lockToPortrait();
            setVisible(false);
          }}
          name="times"
          type="font-awesome"
          color="#EA2027"
        />
      </View>
      {stream.length > 0 && (
        <WebView
          scalesPageToFit={true}
          bounces={false}
          javaScriptEnabled
          style={{height: '100%', width: '100%'}}
          source={{
            uri: stream,
          }}
          automaticallyAdjustContentInsets={false}
        />
      )}
    </Overlay>
  );

  return (
    <SafeAreaView style={styles.container}>
      {ModalSession()}
      <View style={styles.container}>
        <View style={styles.cardUsers}>
          <Text style={{fontWeight: 'bold', fontSize: 18}}>
            Projet {route.params.projectName}
          </Text>
          <View>
            <Text>
              {usersProject.all && Object.values(usersProject.all).length}{' '}
              collaborateurs
            </Text>
          </View>
          <ScrollView style={{marginTop: 10}}>
            {usersProject.all &&
              Object.values(usersProject.all).map((usr: any, i) => (
                <ListItem key={i.toString()}>
                  <Icon
                    name={usr.master ? 'crown' : 'user'}
                    size={12}
                    color="#f1c40f"
                    type="font-awesome-5"
                  />
                  <ListItem.Content>
                    <ListItem.Title style={styles.cardParticipantsText}>
                      {`${usr.pseudo} (${
                        usr.online ? 'en ligne' : 'hors ligne'
                      })`}
                    </ListItem.Title>
                    <ListItem.Subtitle>{usr.email}</ListItem.Subtitle>
                  </ListItem.Content>
                </ListItem>
              ))}
          </ScrollView>
        </View>

        <View style={styles.card}>
          {!sessions.active ? (
            <View style={styles.header}>
              <Text style={{fontWeight: 'bold', fontSize: 18}}>
                Session hors ligne
              </Text>
              <Icon name="low-vision" type="font-awesome" color="#EA2027" />
            </View>
          ) : (
            <>
              <View style={styles.header}>
                <Text style={{fontWeight: 'bold', fontSize: 18}}>
                  Session en cours
                </Text>
                <Badge
                  status="error"
                  badgeStyle={{width: 15, height: 15, borderRadius: 50}}
                />
              </View>
              <View>
                <Text>{participants.session} participants</Text>
              </View>
              <ScrollView
                style={{
                  marginTop: 25,
                }}>
                {sessions.users &&
                  Object.values(sessions.users).map((usr: any, i) => (
                    <ListItem key={i.toString()}>
                      <Icon
                        name={usr.master ? 'crown' : 'user'}
                        size={12}
                        color="#f1c40f"
                        type="font-awesome-5"
                      />
                      <ListItem.Content>
                        <ListItem.Title style={styles.cardParticipantsText}>
                          {usr.pseudo}
                        </ListItem.Title>
                        <ListItem.Subtitle>{usr.email}</ListItem.Subtitle>
                      </ListItem.Content>
                    </ListItem>
                  ))}
              </ScrollView>
              <View style={{flex: 1, justifyContent: 'flex-end'}}>
                <Pressable
                  android_ripple={{color: '#BA49A6'}}
                  style={[styles.button, styles.buttonJoin]}
                  onPress={() => {
                    rejoindreLaSession(sessions.active.uuid);
                    Orientation.lockToLandscape();
                    setVisible(true);
                  }}>
                  <Text style={styles.textStyle}>Rejoindre</Text>
                </Pressable>
              </View>
            </>
          )}
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  card: {
    flex: 1,
    marginTop: 10,
    marginBottom: 25,
    alignSelf: 'center',
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 15,
    padding: 15,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },
  cardUsers: {
    flex: 1,
    marginTop: 10,
    marginBottom: 10,
    alignSelf: 'center',
    height: 250,
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 15,
    padding: 15,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },

  cardParticipantsText: {
    color: '#302D42',
    fontWeight: 'bold',
  },

  button: {
    minWidth: 100,
    borderRadius: 5,
    padding: 10,
    elevation: 2,
  },
  buttonJoin: {
    backgroundColor: '#CA48F6',
    marginRight: 10,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default ProjectScreen;
