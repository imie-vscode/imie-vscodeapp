/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';
import {ListItem} from 'react-native-elements';
import axios from 'axios';
import {API_BASE_URL} from '../../config';
import {NotifyMessage, HandleError} from '../utils/HandleMessage';
import {useCookies} from 'react-cookie';
import {LinesLoader} from 'react-native-indicator';
type listLink =
  | "Politique d'utilisation des données"
  | "Conditions générales d'utilisation"
  | 'Conditions générales de vente'
  | 'Récupérer mes données personnelles';

const AboutScreen = ({navigation}: any) => {
  const [cookie] = useCookies(['user']);
  const headers = {authorization: `Bearer ${cookie.user.token}`};
  const [waiting, setWaiting] = useState(false);

  const list = [
    {
      title: "Politique d'utilisation des données",
    },
    {
      title: "Conditions générales d'utilisation",
    },
    {
      title: 'Conditions générales de vente',
    },
    {
      title: 'Récupérer mes données personnelles',
    },
  ];

  const getpersonnalData = async () => {
    // on affiche le loader
    setWaiting(true);
    try {
      const {
        data,
      } = await axios.get(
        `${API_BASE_URL}/user/${cookie.user.user._id}/personnal-data`,
        {headers},
      );
      NotifyMessage(data.message);
    } catch (error) {
      console.log('error: ', error);
      HandleError(
        'Erreur',
        error.response?.data.error ||
          'Erreur lors de la récupération des données personnelles',
        error.response?.status || 500,
      );
    }
    // on enlève le loader
    setWaiting(false);
  };

  const onClick = (name: listLink) => {
    // si l'on souhaite récupérer nos données, on entre dans cette condition car c'est une action et non une page
    if (name === 'Récupérer mes données personnelles') {
      return getpersonnalData();
    }
    navigation.navigate(name);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={{width: '100%', marginTop: 10}}>
        {list.map((item, i) => (
          <ListItem
            onPress={() => onClick(item.title as listLink)}
            key={i}
            bottomDivider
            pad={30}>
            <ListItem.Content>
              <ListItem.Title style={{color: 'black'}}>
                {item.title}
              </ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron size={25} />
          </ListItem>
        ))}
      </View>
      {waiting && (
        <View style={{marginTop: 25}}>
          <LinesLoader color="#CA48F6" />
        </View>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
});

export default AboutScreen;
