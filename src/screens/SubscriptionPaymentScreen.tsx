/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {
  Pressable,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import {Card, Icon, ListItem} from 'react-native-elements';
import {useCookies} from 'react-cookie';
import {NotifyMessage, HandleError} from '../utils/HandleMessage';
import {ScrollView} from 'react-native-gesture-handler';
import {LinesLoader} from 'react-native-indicator';
import axios from 'axios';
import moment from 'moment';
import {API_BASE_URL} from '../../config';
import {formatPrice} from '../utils/formators';
import LottieView from 'lottie-react-native';
moment.locale('fr');

interface ISubscription {
  _id: string;
  name: string;
  description: string;
  price: string;
  createdAt: Date;
  updatedAt: Date;
  advantages: string[];
}
const SubscriptionPaymentScreen = ({navigation, route}: any) => {
  const [cookie, setCookie] = useCookies(['user']);
  const headers = {authorization: `Bearer ${cookie.user.token}`};
  const [subscription, setSubscription] = useState<ISubscription | null>(null);
  const [disabled, setDisabled] = useState(true);
  const [payment, setPayment] = useState({
    number: '',
    expmonth: '',
    expyear: '',
    cvc: '',
  });
  const [waiting, setWaiting] = useState(true);
  const [paymentWaiting, setPaymentWaiting] = useState(false);
  const [isPaid, setIsPaid] = useState(false); // quand la réponse de l'API pour le paiement de l'abonnement est validé

  useEffect(() => {
    (async () => {
      try {
        const {data} = await axios.get(
          `${API_BASE_URL}/subscription/${route.params._id}`,
        );
        setSubscription(data);
        // on enlève le loader à la fin de la requête si elle réussie
        setWaiting(false);
      } catch (error) {
        //TODO: demander la meilleure façon de gérer des erreurs sur mobile
        console.log('error: ', error);
        HandleError(
          'Erreur',
          error.response?.data.error ||
            'Erreur lors de la récupération des abonnements',
          error.response?.status || 500,
        );
      }
    })();
  }, []);

  // tests carte de crédit
  useEffect(() => {
    let error = 0;
    if (!/^[0-9]{16}$/.test(payment.number)) {
      error++;
    }
    if (!/^(0[1-9]|1[0-2])$/.test(payment.expmonth)) {
      error++;
    }
    if (!/^2[0-9]{3}$/.test(payment.expyear)) {
      error++;
    }
    if (!/^[0-9]{3}$/.test(payment.cvc)) {
      error++;
    }
    error === 0 ? setDisabled(false) : setDisabled(true);
  }, [payment]);

  const subscribe = async () => {
    setPaymentWaiting(true);
    try {
      const dataToSend = {
        card_number: payment.number,
        exp: `${payment.expmonth}/${payment.expyear}`,
        cvc: payment.cvc,
        subscription_id: subscription?._id,
      };
      const {data} = await axios.put(
        `${API_BASE_URL}/user/${cookie.user.user._id}/subscription`,
        dataToSend,
        {
          headers,
        },
      );
      setPaymentWaiting(false);
      setIsPaid(true);
      // on met à jour le cookie avec les nouvelles infos
      // on veille à bien garder le token
      const userCookie = cookie.user;
      setCookie('user', {...userCookie, user: data.user});
      NotifyMessage(data.message);
    } catch (error) {
      console.log('error: ', error);
      setPaymentWaiting(false);
      HandleError(
        'Erreur',
        error.response?.data.error ||
          "Erreur lors de la résiliation de l'abonnement",
        error.response?.status || 500,
      );
    }
  };

  // si l'abonnement n'est pas encore récupéré on affiche un loader
  if (waiting) {
    return (
      <SafeAreaView
        style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <LinesLoader color="#CA48F6" />
      </SafeAreaView>
    );
  }
  if (isPaid) {
    return (
      <SafeAreaView
        style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <LottieView
          source={require('../success-animation.json')}
          autoPlay
          loop={false}
        />
        <Pressable
          onPress={() => navigation.popToTop()}
          android_ripple={{color: 'white'}}
          style={[
            styles.button,
            {width: '75%', position: 'absolute', bottom: 25},
          ]}>
          <Text style={styles.buttonText}>Retourner vers mon profil</Text>
        </Pressable>
      </SafeAreaView>
    );
  }
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={{width: '100%'}}>
        {subscription && (
          <View style={{marginBottom: 25}}>
            <Card containerStyle={styles.cardContainer}>
              {/* Titre de l'abonnement  */}
              <View style={styles.cardTitleContainer}>
                <Card.Title style={styles.cardTitle}>
                  {`Abonnement ${subscription.name}`}
                </Card.Title>
                <Card.Title style={styles.cardPrice}>
                  {subscription.price + ' € /mois'}
                </Card.Title>
              </View>
              <Card.Divider style={{backgroundColor: 'white'}} />

              {/* Avantages de l'abonnement  */}

              {subscription.advantages.map((advantage: string, i: number) => (
                <ListItem
                  key={i.toString()}
                  containerStyle={styles.cardAdvantagesContainer}>
                  <Icon
                    name="star"
                    size={10}
                    color="#f1c40f"
                    type="font-awesome"
                  />
                  <ListItem.Content>
                    <ListItem.Title style={{color: 'white'}}>
                      {advantage}
                    </ListItem.Title>
                  </ListItem.Content>
                </ListItem>
              ))}

              {/* Message de la dernière mise à jour de l'abonnement */}
              <View style={{marginTop: 15}}>
                <Text style={styles.cardSubscribedSince}>
                  {'Dernière mise à jour le ' +
                    moment(subscription.updatedAt).format('LL')}
                </Text>
              </View>
            </Card>

            {/* RECAPITULATIF DE LA COMMANDE */}
            <View style={{padding: 15}}>
              <Text
                style={{
                  fontSize: 16,
                  color: '#302d42',
                  textDecorationStyle: 'solid',
                  textDecorationLine: 'underline',
                }}>
                Récapitulatif de votre commande
              </Text>
              {/* NOM DE L'ABONNEMENT AVEC SON TARIF TTC */}
              <ListItem>
                <Icon
                  name="circle"
                  size={10}
                  color="#f1c40f"
                  type="font-awesome"
                />
                <ListItem.Content>
                  <ListItem.Title>{`Abonnement ${subscription?.name}`}</ListItem.Title>
                  <ListItem.Subtitle>
                    {subscription?.price + ' € /mois TTC'}
                  </ListItem.Subtitle>
                </ListItem.Content>
              </ListItem>

              {/* MONTANT DES TAXES */}
              <View>
                <Text style={styles.textTva}>TVA 5,5%</Text>
                <Text style={styles.textTva}>
                  {formatPrice(
                    Number(subscription?.price.replace(',', '.')) * 0.055,
                  ) + ' €'}
                </Text>
              </View>

              {/* ECHEANCES */}
              <ListItem>
                <Icon
                  name="calendar"
                  color="#EA2027"
                  size={10}
                  type="font-awesome"
                />
                <ListItem.Content>
                  <ListItem.Title>Première échéance</ListItem.Title>
                  <ListItem.Subtitle>
                    {moment(new Date()).format('LL')}
                  </ListItem.Subtitle>
                </ListItem.Content>
              </ListItem>
            </View>
            {/* PAIEMENT */}
            <Card
              containerStyle={{
                backgroundColor: '#302d42',
                borderRadius: 5,
                borderWidth: 0,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  flex: 1,
                  justifyContent: 'space-between',
                  marginBottom: 10,
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 18,
                  }}>
                  Moyens de paiement
                </Text>
                <View style={{flexDirection: 'row'}}>
                  <Icon
                    name="cc-visa"
                    size={25}
                    color="white"
                    type="font-awesome"
                    style={{marginRight: 5}}
                  />
                  <Icon
                    name="cc-mastercard"
                    size={25}
                    color="white"
                    type="font-awesome"
                  />
                </View>
              </View>
              <Card.Divider />
              <TextInput
                onChangeText={(value: string) =>
                  setPayment(prev => ({...prev, number: value}))
                }
                maxLength={16}
                textContentType="creditCardNumber"
                keyboardType="numeric"
                value={payment.number}
                style={styles.textInput}
                placeholder="Numéro de la carte"
              />
              <View
                style={{
                  marginTop: 10,
                  marginBottom: 10,
                  display: 'flex',
                  flexDirection: 'row',
                }}>
                <View style={{flexDirection: 'row', marginRight: 5, flex: 1}}>
                  <TextInput
                    onChangeText={(value: string) =>
                      setPayment(prev => ({...prev, expmonth: value}))
                    }
                    maxLength={2}
                    keyboardType="numeric"
                    value={payment.expmonth}
                    style={[styles.textInput]}
                    placeholder="MM"
                  />
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 18,
                      textAlignVertical: 'center',
                      marginLeft: 2.5,
                      marginRight: 2.5,
                    }}>
                    /
                  </Text>
                  <TextInput
                    onChangeText={(value: string) =>
                      setPayment(prev => ({...prev, expyear: value}))
                    }
                    maxLength={4}
                    keyboardType="numeric"
                    value={payment.expyear}
                    style={[styles.textInput]}
                    placeholder="AAAA"
                  />
                </View>

                <TextInput
                  onChangeText={(value: string) =>
                    setPayment(prev => ({...prev, cvc: value}))
                  }
                  maxLength={3}
                  keyboardType="numeric"
                  value={payment.cvc}
                  style={[styles.textInput, {marginLeft: 5}]}
                  placeholder="CVC"
                />
              </View>
              {/* Quand on clique sur le bouton payer, on affiche à la place du bouton un loader pour signifier l'attente du paiement */}
              {paymentWaiting ? (
                <View style={{alignItems: 'center', marginTop: 25}}>
                  <LinesLoader color="#CA48F6" />
                </View>
              ) : (
                <Pressable
                  android_ripple={{color: 'white'}}
                  onPress={subscribe}
                  disabled={disabled}
                  style={[styles.button, {opacity: disabled ? 0.5 : 1}]}>
                  <Text
                    style={
                      styles.buttonText
                    }>{`Payer (${subscription?.price} €)`}</Text>
                </Pressable>
              )}
            </Card>
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  cardContainer: {
    borderWidth: 0,
    borderRadius: 5,
    backgroundColor: '#302d42',
    marginBottom: 25,
  },
  cardTitleContainer: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
  },
  cardTitle: {fontSize: 20, fontWeight: 'bold', color: 'white'},
  cardPrice: {
    backgroundColor: '#27ae60',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 7.5,
    paddingRight: 7.5,
    color: 'white',
    borderRadius: 5,
  },
  cardSubscribedSince: {
    textAlign: 'right',
    color: 'white',
    fontStyle: 'italic',
  },
  cardAdvantagesContainer: {
    backgroundColor: '#302d42',
    padding: 0,
    marginBottom: 10,
  },

  button: {
    marginTop: 25,
    backgroundColor: '#F8AE6B',
    width: '100%',
    borderWidth: 0,
    borderRadius: 5,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
  },

  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 17,
    textAlign: 'center',
  },
  textTva: {
    textAlign: 'right',
    fontSize: 14,
    color: 'silver',
    fontStyle: 'italic',
  },
  textInput: {
    flex: 1,
    backgroundColor: 'white',
    borderWidth: 0,
    padding: 5,
    borderRadius: 5,
    textAlign: 'center',
  },
});

export default SubscriptionPaymentScreen;
