/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useContext, useState} from 'react';
import {
  Modal,
  Pressable,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {Avatar, CheckBox, ListItem} from 'react-native-elements';
import {useCookies} from 'react-cookie';
import axios from 'axios';
import {API_BASE_URL} from '../../config';
import {NotifyMessage, HandleError} from '../utils/HandleMessage';
import {LoginManager} from 'react-native-fbsdk';
import {LinesLoader} from 'react-native-indicator';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import {SocketContext} from '../context/socket';

type listLink =
  | 'Modifier mon profil'
  | 'Abonnement - factures'
  | 'A propos'
  | 'Se déconnecter'
  | 'Supprimer mon compte';
const ProfileScreen = ({navigation}: any) => {
  const [cookie, , removeCookie] = useCookies([
    'user',
    'facebook',
    'google',
    'salons',
  ]);
  const headers = {authorization: `Bearer ${cookie.user.token}`};
  const [modalVisible, setModalVisible] = useState(false);
  const [getAllpersonnalData, setGetAllPersonnalData] = useState(false);
  const [removeWaiting, setRemoveWaiting] = useState(false);
  const socket = useContext(SocketContext);

  const disconnect = () => {
    // on vérifie si l'utilisateur s'est connecté avec facebook
    const fbCookie = cookie.facebook;
    const googleCookie = cookie.google;
    if (fbCookie === '1') {
      removeCookie('facebook');
      LoginManager.logOut();
    }
    if (googleCookie === '1') {
      removeCookie('google');
      GoogleSignin.signOut();
    }

    // on quitte les salons
    quitterLeSalon();
    removeCookie('user');
  };

  const quitterLeSalon = () => {
    // on map sur tous les salons qu'on a rejoint
    const salons = cookie.salons;
    if (!salons) {
      return;
    }
    for (const salon of salons) {
      socket.emit('LEAVE_ROOM', {
        id_project: salon,
        id_user: cookie.user.user._id,
      });
    }

    removeCookie('salons');
  };

  const removeAccount = async () => {
    setRemoveWaiting(true);
    try {
      const {data} = await axios.delete(
        `${API_BASE_URL}/user/${cookie.user.user._id}`,
        {
          headers,
          params: {
            personnal_data: getAllpersonnalData ? 'checked' : 'unchecked',
          },
        },
      );
      NotifyMessage(data.message);
      setTimeout(() => {
        disconnect();
      }, 2000);
    } catch (error: any) {
      console.log('error: ', error);
      HandleError(
        'Erreur',
        error.response?.data.error || 'Erreur lors de la suppression du profil',
        error.response?.status || 500,
      );
    }
    setRemoveWaiting(false);
  };

  const list = [
    {
      title: 'Modifier mon profil',
      icon: 'edit',
    },
    {
      title: 'Abonnement - factures',
      icon: 'wallet',
    },
    {
      title: 'A propos',
      icon: 'info',
    },
    {
      title: 'Se déconnecter',
      icon: 'sign-out-alt',
    },
    {
      title: 'Supprimer mon compte',
      icon: 'sign-out-alt',
    },
  ];

  const onClick = (name: listLink) => {
    switch (name) {
      case 'Se déconnecter':
        disconnect();
        break;
      case 'Supprimer mon compte':
        setModalVisible(true);
        break;
      default:
        navigation.navigate(name);
        break;
    }
  };

  return (
    <SafeAreaView
      style={[
        styles.container,
        {
          opacity: modalVisible ? 0.2 : 1,
        },
      ]}>
      <View style={styles.header}>
        <Avatar
          containerStyle={{
            position: 'absolute',
            top: 25,
            opacity: modalVisible ? 0.2 : 1,
          }}
          size="xlarge"
          rounded
          source={require('../../images/pp.png')}
        />
      </View>
      {cookie.user && (
        <View style={styles.pseudoContainer}>
          <Text style={styles.pseudoText}>{cookie.user.user.pseudo}</Text>
        </View>
      )}

      <View style={{width: '100%', marginTop: 10}}>
        <Modal
          transparent
          animationType="slide"
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={styles.modalText}>
                Voulez-vous vraiment supprimer votre compte ?
              </Text>
              <View style={styles.checkboxContainer}>
                <CheckBox
                  checked={getAllpersonnalData}
                  onPress={() => setGetAllPersonnalData(!getAllpersonnalData)}
                  title="Je veux récupérer mes données personnelles"
                  style={styles.checkbox}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 25,
                }}>
                {/* ATTENDRE LA FIN DE LA SUPPRESSION DU COMPTE */}
                {removeWaiting ? (
                  <View
                    style={{
                      marginTop: 25,
                      alignItems: 'center',
                    }}>
                    <LinesLoader color="#CA48F6" />
                  </View>
                ) : (
                  <>
                    <Pressable
                      style={[styles.button, styles.buttonConfirm]}
                      onPress={removeAccount}>
                      <Text style={styles.textStyle}>Oui</Text>
                    </Pressable>
                    <Pressable
                      style={[styles.button, styles.buttonClose]}
                      onPress={() => setModalVisible(false)}>
                      <Text style={styles.textStyle}>Annuler</Text>
                    </Pressable>
                  </>
                )}
              </View>
            </View>
          </View>
        </Modal>

        {list.map((item, i) => (
          <ListItem
            onPress={() => onClick(item.title as listLink)}
            key={i}
            bottomDivider
            pad={30}
            containerStyle={{
              backgroundColor:
                item.title === 'Supprimer mon compte' ? 'red' : 'white',
            }}>
            <ListItem.Content>
              <ListItem.Title
                style={{
                  color:
                    item.title === 'Se déconnecter'
                      ? 'red'
                      : item.title === 'Supprimer mon compte'
                      ? 'white'
                      : 'black',
                  fontWeight:
                    item.title === 'Se déconnecter'
                      ? 'bold'
                      : item.title === 'Supprimer mon compte'
                      ? 'bold'
                      : 'normal',
                }}>
                {item.title}
              </ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron
              size={25}
              color={item.title === 'Supprimer mon compte' ? 'white' : 'silver'}
            />
          </ListItem>
        ))}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  header: {
    height: 100,
    backgroundColor: '#302d42',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pseudoContainer: {marginTop: 80},
  pseudoText: {fontSize: 20, fontWeight: 'bold', color: '#302d42'},
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 5,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    minWidth: 100,
    borderRadius: 5,
    padding: 10,
    elevation: 2,
  },
  buttonConfirm: {
    backgroundColor: '#CA48F6',
    marginRight: 10,
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    fontSize: 18,
    marginBottom: 15,
    textAlign: 'center',
  },
  checkboxContainer: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: 'center',
  },
});

export default ProfileScreen;
