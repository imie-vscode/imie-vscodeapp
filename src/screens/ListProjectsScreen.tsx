/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {Icon} from 'react-native-elements';
import axios from 'axios';
import {API_BASE_URL} from '../../config';
import {HandleError} from '../utils/HandleMessage';
import {useCookies} from 'react-cookie';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useIsFocused} from '@react-navigation/native';
import {FlatGrid} from 'react-native-super-grid';
import LottieView from 'lottie-react-native';

type TemplateType = 'REACT' | 'VUE' | 'ANGULAR';
interface IItem {
  name: string;
  icon: string;
  template: string;
  color: string;
  _id: string;
}
const ListProjectsScreen = ({navigation}: any) => {
  const [cookie] = useCookies(['user']);
  const headers = {authorization: `Bearer ${cookie.user.token}`};
  const isFocused = useIsFocused();
  const [items, setItems] = useState<IItem[]>([]);
  const [waiting, setWaiting] = useState(true);

  useEffect(() => {
    isFocused && fetchProjects();
  }, [navigation, isFocused]);

  const fetchProjects = async () => {
    try {
      const {
        data,
      } = await axios.get(
        `${API_BASE_URL}/user/${cookie.user.user._id}/project`,
        {headers},
      );
      const dataFormated = data.map((val: any) => {
        let icon = '';
        let color = '';
        switch (val.template as TemplateType) {
          case 'REACT':
            icon = 'react';
            color = '#3498db';
            break;
          case 'VUE':
            icon = 'vuejs';
            color = '#2ecc71';
            break;
          case 'ANGULAR':
            icon = 'angular';
            color = '#EA2027';
            break;
          default:
            icon = 'terminal';
            color = 'black';
            break;
        }
        return {
          name: val.name,
          template: val.template,
          icon,
          color,
          _id: val._id,
        };
      });

      setItems(dataFormated);
      setWaiting(false);
    } catch (error) {
      console.log('error: ', error);
      HandleError(
        'Erreur',
        error.response?.data.error ||
          'Erreur lors de la récupération des projets',
        error.response?.status || 500,
      );
    }
  };

  const onClick = (_id: string, projectName: string) => {
    navigation.navigate('Projet', {_id, projectName});
  };

  if (waiting) {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <LottieView source={require('../loader.json')} autoPlay loop />
      </SafeAreaView>
    );
  }
  return (
    <FlatGrid
      itemDimension={130}
      data={items}
      style={styles.gridView}
      // staticDimension={300}
      // fixed
      spacing={10}
      renderItem={({item}) => (
        <TouchableOpacity
          onPress={() => onClick(item._id, item.name)}
          style={[
            styles.itemContainer,
            {
              backgroundColor: 'white',
              borderColor: '#302d42',
              borderWidth: 0,
              borderRadius: 5,
              shadowColor: '#000',
              shadowOffset: {
                width: 1,
                height: 5,
              },
              shadowOpacity: 0.25,
              shadowRadius: 4,
              elevation: 5,
            },
          ]}>
          <Icon
            name={item.icon}
            type="font-awesome-5"
            color={item.color}
            size={50}
          />
          <View>
            <Text style={styles.itemName}>{item.name}</Text>
            <Text style={styles.itemTemplate}>{item.template}</Text>
          </View>
        </TouchableOpacity>
      )}
    />
  );
};

const styles = StyleSheet.create({
  gridView: {
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'space-between',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 18,
    color: '#302d42',
    fontWeight: 'bold',
  },
  itemTemplate: {
    fontSize: 16,
    color: 'silver',
  },
});

export default ListProjectsScreen;
