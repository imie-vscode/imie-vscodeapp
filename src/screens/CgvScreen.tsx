/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {SafeAreaView, StyleSheet, Text} from 'react-native';
import content from '../utils/CgvContent';
import HTMLView from 'react-native-htmlview';
import {ScrollView} from 'react-native-gesture-handler';
const CgvScreen = ({navigation}: any) => {
  return (
    <SafeAreaView style={{backgroundColor: 'white'}}>
      <Text
        style={{
          fontSize: 22,
          fontWeight: 'bold',
          textAlign: 'center',
          color: '#302d42',
          marginBottom: 25,
          marginTop: 25,
        }}>
        Conditions générales de vente
      </Text>
      <ScrollView style={{padding: 15}}>
        <HTMLView value={content} stylesheet={styles} />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  a: {
    fontWeight: '300',
    color: '#FF3366', // make links coloured pink
  },
  h1: {fontSize: 20},
  h2: {fontSize: 18},
  h3: {fontSize: 16},
});

export default CgvScreen;
