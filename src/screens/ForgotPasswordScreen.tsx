/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {
  Pressable,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import axios from 'axios';
import {API_BASE_URL} from '../../config';
import {isEmail} from '../utils/Validators';
import {HandleError} from '../utils/HandleMessage';
import {LinesLoader} from 'react-native-indicator';
import LottieView from 'lottie-react-native';

const ForgotPasswordScreen = ({navigation}: {navigation: any}) => {
  const [email, setEmail] = useState('');
  const [emailSent, setEmailSent] = useState(false);
  const [waiting, setWaiting] = useState(false);
  const [disabled, setDisabled] = useState(true);

  useEffect(() => {
    let error = 0;
    if (!isEmail(email)) {
      error++;
    }

    error === 0 ? setDisabled(false) : setDisabled(true);
  }, [email]);

  const onSubmit = async () => {
    setWaiting(true);
    try {
      const {data} = await axios.post(`${API_BASE_URL}/auth/forgot-password`, {
        email,
      });
      setWaiting(false);
      setEmailSent(true);
    } catch (error) {
      setWaiting(false);
      console.log('error: ', error);
      HandleError(
        'Erreur',
        error.response?.data.error || 'Erreur serveur',
        error.response?.status || 500,
      );
    }
  };

  if (emailSent) {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white',
          padding: 15,
        }}>
        <LottieView
          source={require('../email-confirmation-sent.json')}
          autoPlay
          loop={false}
        />
        <Pressable
          onPress={() => navigation.popToTop()}
          android_ripple={{color: 'white'}}
          style={[
            styles.button,
            {width: '75%', position: 'absolute', bottom: 25},
          ]}>
          <Text style={styles.buttonText}>Retour</Text>
        </Pressable>
      </SafeAreaView>
    );
  }

  return (
    <SafeAreaView style={[styles.container]}>
      <View style={{flex: 1}}>
        <Text style={styles.title}>Veuillez entrer votre courriel</Text>
        <TextInput
          onChangeText={(value: string) => setEmail(value)}
          textContentType="emailAddress"
          keyboardType="email-address"
          value={email}
          style={styles.textInput}
          placeholder="courriel@gmail.com"
        />

        {/* Quand on clique sur le bouton valider, on affiche un loader en attendant la réponse serveur */}
        {waiting ? (
          <View style={{alignItems: 'center', marginTop: 25}}>
            <LinesLoader color="#CA48F6" />
          </View>
        ) : (
          <Pressable
            android_ripple={{color: 'white'}}
            onPress={onSubmit}
            disabled={disabled}
            style={[styles.button, {opacity: disabled ? 0.5 : 1}]}>
            <Text style={styles.buttonText}>Valider</Text>
          </Pressable>
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#302d42',
    padding: 25,
  },
  title: {fontSize: 16, fontWeight: 'bold', color: 'white'},
  button: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#F8AE6B',
    width: '100%',
    borderWidth: 0,
    borderRadius: 5,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
  },

  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 17,
    textAlign: 'center',
  },

  textInput: {
    marginTop: 15,
    backgroundColor: 'white',
    borderWidth: 0,
    padding: 5,
    borderRadius: 5,
    textAlign: 'center',
  },
});

export default ForgotPasswordScreen;
