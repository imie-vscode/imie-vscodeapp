/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {
  Modal,
  Pressable,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {CheckBox, Input} from 'react-native-elements';

import axios from 'axios';
import {API_BASE_URL} from '../../config';
import {HandleError} from '../utils/HandleMessage';
import {useCookies} from 'react-cookie';
import {ScrollView} from 'react-native-gesture-handler';
import LottieView from 'lottie-react-native';
import {LinesLoader} from 'react-native-indicator';
import {SEXES} from '../utils/Constantes';
import CguComponent from '../components/CguComponent';
import {isExisted, isNumeric, isPostalCode} from '../utils/Validators';
import SwitchSelector from 'react-native-switch-selector';

type Services = 'GOOGLE' | 'FACEBOOK' | 'GITHUB';
const ComplementaryFormScreen = ({route}: any) => {
  const [, setCookie] = useCookies(['user']);
  const [modalVisible, setModalVisible] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const [waiting, setWaiting] = useState(false);
  const [isRegistered, setIsRegistered] = useState(false); // quand la réponse de l'API pour l'inscription validée
  const [email, setEmail] = useState(''); // courriel reçu par le service que l'on ne peut pas modifier
  const [state, setState] = useState({
    service: '' as Services,
    facebookId: '',
    googleId: '',
    githubId: '',
    firstname: '',
    lastname: '',
    pseudo: '',
    num_address: '',
    city: '',
    address: '',
    country: '',
    postal_code: '',
    sexe: SEXES[0].value,
    newsletter: true,
    birthday: {
      day: '',
      month: '',
      year: '',
    },
  });
  const [acceptPolicies, setAcceptPolicies] = useState(false);

  useEffect(() => {
    (async () => {
      const data = route.params.user;
      const service = route.params.service as Services;

      if (service === 'GOOGLE') {
        setEmail(data.email);
        setState({
          ...state,
          service,
          firstname: data.givenName || state.firstname,
          lastname: data.familyName || state.lastname,
          googleId: data.id,
        });
        return;
      }
      if (service === 'FACEBOOK') {
        setEmail(data.email);
        setState({
          ...state,
          service,
          firstname: data.firstname,
          lastname: data.lastname,
          facebookId: data.id,
          birthday: data.birthday,
          city: data.city,
        });
        return;
      }
      if (service === 'GITHUB') {
        setEmail(data.email);
        setState({
          ...state,
          service,
          firstname: data.firstname,
          githubId: data.id,
          pseudo: data.pseudo,
        });
        return;
      }
    })();
  }, [route]);

  // vérification des champs
  useEffect(() => {
    let error = 0;

    // #############################
    // vérifications si les champs existent
    // #############################
    if (!isExisted(state.sexe)) error++;
    if (!isExisted(state.firstname)) error++;
    if (!isExisted(state.lastname)) error++;
    if (!isExisted(state.pseudo)) error++;
    if (!isExisted(state.address)) error++;
    if (!isExisted(state.city)) error++;
    if (!isExisted(state.num_address)) error++;
    if (!isExisted(state.postal_code)) error++;
    if (!isExisted(state.country)) error++;
    if (!isExisted(state.birthday.day)) error++;
    if (!isExisted(state.birthday.month)) error++;
    if (!isExisted(state.birthday.year)) error++;

    // #############################
    //   vérification des formats
    // #############################
    if (!isNumeric(state.num_address)) error++;
    if (!isNumeric(state.birthday.day)) error++;
    if (!isNumeric(state.birthday.month)) error++;
    if (!isNumeric(state.birthday.year)) error++;
    if (!isPostalCode(state.postal_code)) error++;

    // #############################
    //     longueur des champs
    // #############################
    if (state.lastname.length > 50) error++;
    if (state.firstname.length > 50) error++;
    if (state.country.length > 50) error++;
    if (state.pseudo.length > 50) error++;
    if (state.num_address.length > 50) error++;
    if (state.address.length > 255) error++;
    if (state.city.length > 255) error++;
    if (state.birthday.day.length > 2) error++;
    if (state.birthday.month.length > 2) error++;
    if (state.birthday.year.length > 4) error++;

    // #############################
    // Conditions générales cochées
    // #############################
    if (!acceptPolicies) error++;

    error === 0 ? setDisabled(false) : setDisabled(true);
  }, [state, acceptPolicies]);

  const onSubmit = async () => {
    setWaiting(true);

    try {
      const dataToSend = {
        ...state,
        email,
        birthday: `${state.birthday.year}-${state.birthday.month}-${state.birthday.day}`,
      };

      // on fait une requête d'inscription sur l'url correspondant au service
      const url =
        state.service === 'GOOGLE'
          ? `${API_BASE_URL}/auth/google`
          : state.service === 'FACEBOOK'
          ? `${API_BASE_URL}/auth/facebook`
          : `${API_BASE_URL}/auth/github`;

      const {data} = await axios.post(url, dataToSend);

      // inscription réussie
      setIsRegistered(true);

      // on attend la fin de l'annimation pour mettre à jour les cookies
      setTimeout(() => {
        setCookie('user', data);
        state.service === 'GOOGLE'
          ? setCookie('google', '1')
          : state.service === 'FACEBOOK'
          ? setCookie('facebook', '1')
          : setCookie('github', '1');
      }, 3500);
    } catch (error) {
      console.log('error: ', error);
      // si l'erreur est un tableau d'erreurs des champs
      if (Array.isArray(error.response.data?.error)) {
        //TODO: trouver une solution pour bien afficher les erreurs de modifications qui sont dans un tableau
        HandleError(
          'Erreur',
          error.response.data.error.join(' - '),
          error.response.status || 500,
        );
      } else {
        HandleError(
          'Erreur',
          error.response.data.error ||
            "Erreur lors de l'inscription, veuillez réessayer",
          error.response.status || 500,
        );
      }
    }
    setWaiting(false);
  };

  if (isRegistered) {
    return (
      <SafeAreaView
        style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <LottieView
          source={require('../registered.json')}
          autoPlay
          loop={false}
        />
      </SafeAreaView>
    );
  }
  return (
    <SafeAreaView
      style={[
        styles.container,
        {
          opacity: modalVisible ? 0.2 : 1,
        },
      ]}>
      <Modal
        transparent
        animationType="slide"
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Pressable
              android_ripple={{color: 'white'}}
              style={styles.exit}
              onPress={() => setModalVisible(false)}>
              <Text
                style={{
                  color: 'white',
                  fontSize: 18,
                }}>
                X
              </Text>
            </Pressable>

            <Text style={{fontSize: 18}}>Conditions d'utilisations</Text>

            <ScrollView style={{marginTop: 25}}>
              <CguComponent />
            </ScrollView>
          </View>
        </View>
      </Modal>
      <ScrollView
        style={{
          width: '100%',
          margin: 15,
        }}>
        {/* #############################
                      Courriel
            ############################# */}
        <View>
          <Text style={styles.label}>Courriel</Text>
          <Input
            style={[styles.input]}
            inputStyle={{color: 'white'}}
            value={email}
            disabled
            leftIcon={{
              type: 'font-awesome',
              name: 'envelope',
              color: 'white',
              size: 17,
            }}
          />
        </View>
        {/* #############################
                      Prénom
            ############################# */}
        <View>
          <Text style={styles.label}>Prénom</Text>
          <Input
            style={styles.input}
            inputStyle={{color: 'white'}}
            maxLength={50}
            value={state.firstname}
            onChangeText={(value: string) =>
              setState(prev => ({...prev, firstname: value}))
            }
            leftIcon={{
              type: 'font-awesome',
              name: 'user',
              color: 'white',
              size: 17,
            }}
          />
        </View>
        {/* #############################
                      Nom
            ############################# */}
        <View>
          <Text style={styles.label}>Nom</Text>
          <Input
            style={styles.input}
            inputStyle={{color: 'white'}}
            maxLength={50}
            value={state.lastname}
            onChangeText={(value: string) =>
              setState(prev => ({...prev, lastname: value}))
            }
            leftIcon={{
              type: 'font-awesome',
              name: 'user',
              color: 'white',
              size: 17,
            }}
          />
        </View>
        {/* #############################
                      Pseudo
            ############################# */}
        <View>
          <Text style={styles.label}>Pseudo</Text>
          <Input
            style={styles.input}
            inputStyle={{color: 'white'}}
            maxLength={50}
            value={state.pseudo}
            onChangeText={(value: string) =>
              setState(prev => ({...prev, pseudo: value}))
            }
            leftIcon={{
              type: 'font-awesome',
              name: 'user',
              color: 'white',
              size: 17,
            }}
          />
        </View>
        {/* #############################
                      Sexe
            ############################# */}
        <View>
          <Text style={styles.label}>Sexe</Text>
          <SwitchSelector
            style={{marginTop: 15}}
            initial={0}
            onPress={(value: string) =>
              setState(prev => ({...prev, sexe: value}))
            }
            textColor="#7a44cf" //'#7a44cf'
            selectedColor="white"
            buttonColor="#7a44cf"
            borderColor="#7a44cf"
            hasPadding
            textStyle={{fontSize: 17, fontWeight: 'bold'}}
            selectedTextStyle={{fontSize: 17, fontWeight: 'bold'}}
            options={SEXES}
          />
        </View>
        {/* #############################
                  Date de naissance
            ############################# */}
        <View style={{marginTop: 25}}>
          <Text style={styles.label}>Date de naissance</Text>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 2}}>
              <Input
                style={styles.input}
                inputStyle={{color: 'white'}}
                keyboardType="numeric"
                value={state.birthday.day}
                placeholder="JJ"
                maxLength={2}
                onChangeText={(value: string) =>
                  setState(prev => ({
                    ...prev,
                    birthday: {...prev.birthday, day: value},
                  }))
                }
              />
            </View>
            <View style={{flex: 2}}>
              <Input
                style={styles.input}
                inputStyle={{color: 'white'}}
                keyboardType="numeric"
                value={state.birthday.month}
                placeholder="MM"
                maxLength={2}
                onChangeText={(value: string) =>
                  setState(prev => ({
                    ...prev,
                    birthday: {...prev.birthday, month: value},
                  }))
                }
              />
            </View>
            <View style={{flex: 2}}>
              <Input
                style={styles.input}
                inputStyle={{color: 'white'}}
                keyboardType="numeric"
                value={state.birthday.year}
                placeholder="AAAA"
                maxLength={4}
                onChangeText={(value: string) =>
                  setState(prev => ({
                    ...prev,
                    birthday: {...prev.birthday, year: value},
                  }))
                }
              />
            </View>
          </View>
        </View>

        {/* #######################################################################################
                                        PARTIE ADRESSE POSTALE
            ####################################################################################### */}
        <View style={{marginTop: 50, alignItems: 'center'}}>
          <Text style={{fontSize: 25, color: 'white'}}>Adresse postale</Text>
        </View>
        {/* #############################
                           N°
            ############################# */}
        <View>
          <Text style={styles.label}>N°</Text>
          <Input
            style={styles.input}
            inputStyle={{color: 'white'}}
            maxLength={50}
            keyboardType="numeric"
            value={state.num_address}
            onChangeText={(value: string) =>
              setState(prev => ({...prev, num_address: value}))
            }
            leftIcon={{
              type: 'font-awesome',
              name: 'map-marker',
              color: 'white',
              size: 17,
            }}
          />
        </View>
        {/* #############################
                      Adresse
            ############################# */}
        <View>
          <Text style={styles.label}>Adresse</Text>
          <Input
            style={styles.input}
            inputStyle={{color: 'white'}}
            maxLength={255}
            value={state.address}
            onChangeText={(value: string) =>
              setState(prev => ({...prev, address: value}))
            }
            leftIcon={{
              type: 'font-awesome',
              name: 'map-marker',
              color: 'white',
              size: 17,
            }}
          />
        </View>
        {/* #############################
                      Ville
            ############################# */}
        <View>
          <Text style={styles.label}>Ville</Text>
          <Input
            style={styles.input}
            inputStyle={{color: 'white'}}
            value={state.city}
            maxLength={255}
            onChangeText={(value: string) =>
              setState(prev => ({...prev, city: value}))
            }
            leftIcon={{
              type: 'font-awesome',
              name: 'map-marker',
              color: 'white',
              size: 17,
            }}
          />
        </View>
        {/* #############################
                      Code postal
            ############################# */}
        <View>
          <Text style={styles.label}>Code postal</Text>
          <Input
            style={styles.input}
            inputStyle={{color: 'white'}}
            keyboardType="numeric"
            value={state.postal_code}
            maxLength={5}
            onChangeText={(value: string) =>
              setState(prev => ({...prev, postal_code: value}))
            }
            leftIcon={{
              type: 'font-awesome',
              name: 'map-marker',
              color: 'white',
              size: 17,
            }}
          />
        </View>
        {/* #############################
                      Pays
            ############################# */}
        <View>
          <Text style={styles.label}>Pays</Text>
          <Input
            style={styles.input}
            inputStyle={{color: 'white'}}
            value={state.country}
            maxLength={50}
            onChangeText={(value: string) =>
              setState(prev => ({...prev, country: value}))
            }
            leftIcon={{
              type: 'font-awesome',
              name: 'globe',
              color: 'white',
              size: 17,
            }}
          />
        </View>
        {/* #############################
                      Newsletter
            ############################# */}
        <CheckBox
          checkedColor="#F8AE6B"
          containerStyle={{backgroundColor: '#302d42', borderWidth: 0}}
          textStyle={{color: 'white', fontSize: 16}}
          checked={state.newsletter}
          onPress={() =>
            setState(prev => ({...prev, newsletter: !prev.newsletter}))
          }
          title="S'inscrire à la Newsletter IMIE VSCode"
          style={styles.checkbox}
        />
        {/* #############################
                        CGU
            ############################# */}
        <View>
          <CheckBox
            checkedColor="#F8AE6B"
            containerStyle={{backgroundColor: '#302d42', borderWidth: 0}}
            textStyle={{color: 'white', fontSize: 16}}
            checked={acceptPolicies}
            onPress={() => setAcceptPolicies(!acceptPolicies)}
            title="J'accepte les conditions d'utilisations"
            style={styles.checkbox}
          />
          <Text
            onPress={() => setModalVisible(true)}
            style={{
              textDecorationStyle: 'solid',
              textDecorationLine: 'underline',
              textAlign: 'center',
              color: 'white',
            }}>
            Voir les conditions
          </Text>
        </View>

        {/* #############################
                        ERREURS
            ############################# */}

        <View style={{marginTop: 25, opacity: acceptPolicies ? 0 : 1}}>
          <Text
            style={{
              textAlign: 'center',
              fontStyle: 'italic',
              fontWeight: 'bold',
              color: 'white',
            }}>
            Veuillez accepter les conditions d'utilisations pour vous inscrire
          </Text>
        </View>

        <View style={{alignItems: 'center', marginBottom: 15}}>
          {waiting ? (
            <View style={{alignItems: 'center', marginTop: 25}}>
              <LinesLoader color="#CA48F6" />
            </View>
          ) : (
            <Pressable
              android_ripple={{color: '#F8AE6B'}}
              disabled={disabled}
              onPress={onSubmit}
              style={[styles.button, {opacity: disabled ? 0.5 : 1}]}>
              <Text style={styles.buttonText}>Enregistrer</Text>
            </Pressable>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#302d42',
    alignItems: 'center',
    padding: 15,
  },
  // MODAL
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    marginTop: 50,
    marginBottom: 50,
    marginLeft: 25,
    marginRight: 25,
    backgroundColor: 'white',
    borderRadius: 5,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  exit: {
    borderWidth: 0,
    position: 'absolute',
    top: 10,
    right: 10,
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#EA2027',
    borderRadius: 50,
    width: 35,
    height: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 5,
  },
  // FIN MODAL
  input: {
    borderWidth: 0,
    fontSize: 15,
  },

  button: {
    marginTop: 25,
    borderColor: '#F8AE6B',
    width: '100%',
    borderWidth: 2,
    borderRadius: 5,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
  },

  buttonText: {
    color: '#F8AE6B',
    fontWeight: 'bold',
    fontSize: 17,
    textAlign: 'center',
  },
  checkbox: {
    alignSelf: 'center',
  },
  label: {
    color: 'white',
    fontSize: 14,
  },
});

export default ComplementaryFormScreen;
