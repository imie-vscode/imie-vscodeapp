/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {Pressable, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {Card, Icon, ListItem} from 'react-native-elements';
import {useCookies} from 'react-cookie';
import {HandleError} from '../utils/HandleMessage';
import {ScrollView} from 'react-native-gesture-handler';
import axios from 'axios';
import moment from 'moment';
import {API_BASE_URL} from '../../config';
moment.locale('fr');

interface ISubscription {
  _id: string;
  name: string;
  description: string;
  price: string;
  createdAt: Date;
  updatedAt: Date;
  advantages: string[];
}
const SubscriptionsScreen = ({navigation}: any) => {
  const [cookie, setCookie] = useCookies(['user']);
  // const headers = {authorization: `Bearer ${cookie.user.token}`};
  const [subscriptions, setSubscriptions] = useState<ISubscription[]>([]);

  useEffect(() => {
    (async () => {
      try {
        const {data} = await axios.get(`${API_BASE_URL}/subscription`);
        setSubscriptions(data);
      } catch (error) {
        console.log('error: ', error);
        HandleError(
          'Erreur',
          error.response?.data.error ||
            'Erreur lors de la récupération des abonnements',
          error.response?.status || 500,
        );
      }
    })();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <Text
        style={{
          fontSize: 22,
          fontWeight: 'bold',
          textAlign: 'center',
          color: '#302d42',
          marginTop: 20,
          marginBottom: 20,
        }}>
        Abonnements IMIE VSCode
      </Text>
      <ScrollView style={{width: '100%'}}>
        {subscriptions.map(subscription => (
          <Card containerStyle={styles.cardContainer} key={subscription._id}>
            {/* Titre de l'abonnement  */}
            <View style={styles.cardTitleContainer}>
              <Card.Title style={styles.cardTitle}>
                {subscription.name}
              </Card.Title>
              <Card.Title style={styles.cardPrice}>
                {subscription.price + ' € /mois'}
              </Card.Title>
            </View>
            <Card.Divider style={{backgroundColor: 'white'}} />

            {/* Avantages de l'abonnement  */}

            {subscription.advantages.map((advantage: string, i: number) => (
              <ListItem
                key={i.toString()}
                containerStyle={styles.cardAdvantagesContainer}>
                <Icon
                  name="star"
                  size={10}
                  color="#f1c40f"
                  type="font-awesome"
                />
                <ListItem.Content>
                  <ListItem.Title style={{color: 'white'}}>
                    {advantage}
                  </ListItem.Title>
                </ListItem.Content>
              </ListItem>
            ))}

            {/* Message de la dernière mise à jour de l'abonnement */}
            <View style={{marginTop: 15}}>
              <Text style={styles.cardSubscribedSince}>
                {'Dernière mise à jour le ' +
                  moment(subscription.updatedAt).format('LL')}
              </Text>
            </View>

            {/* Boutton  */}
            <Pressable
              android_ripple={{color: 'white'}}
              style={styles.buttonChooseSubscription}
              onPress={() =>
                navigation.navigate('Abonnement paiement', {
                  _id: subscription._id,
                })
              }>
              <Text style={styles.buttonText}>Choisir</Text>
            </Pressable>
          </Card>
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  cardContainer: {
    borderWidth: 0,
    borderRadius: 5,
    backgroundColor: '#302d42',
    marginBottom: 25,
  },
  cardTitleContainer: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
  },
  cardTitle: {fontSize: 20, fontWeight: 'bold', color: 'white'},
  cardPrice: {
    backgroundColor: '#27ae60',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 7.5,
    paddingRight: 7.5,
    color: 'white',
    borderRadius: 5,
  },
  cardSubscribedSince: {
    textAlign: 'right',
    color: 'white',
    fontStyle: 'italic',
  },
  cardAdvantagesContainer: {
    backgroundColor: '#302d42',
    padding: 0,
    marginBottom: 10,
  },

  buttonChooseSubscription: {
    marginTop: 25,
    backgroundColor: '#F8AE6B',
    width: '100%',
    borderWidth: 0,
    borderRadius: 5,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 15,
    paddingRight: 15,
  },

  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 17,
    textAlign: 'center',
  },
});

export default SubscriptionsScreen;
