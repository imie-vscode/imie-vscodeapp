/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Accessory, Avatar, Input} from 'react-native-elements';

import axios from 'axios';
import {API_BASE_URL} from '../../config';
import {HandleError, NotifyMessage} from '../utils/HandleMessage';
import {useCookies} from 'react-cookie';
import {ScrollView} from 'react-native-gesture-handler';

const EditProfileScreen = ({route}: any) => {
  const [cookie] = useCookies(['user']);
  const headers = {authorization: `Bearer ${cookie.user.token}`};
  const [state, setState] = useState({
    firstname: '',
    lastname: '',
    pseudo: '',
    num_address: '',
    city: '',
    address: '',
    country: '',
    postal_code: '',
    sexe: '',
    birthday: {
      day: '',
      month: '',
      year: '',
    },
  });

  useEffect(() => {
    (async () => {
      try {
        const {data} = await axios.get(
          `${API_BASE_URL}/user/${cookie.user.user._id}`,
          {
            headers,
          },
        );

        // calcul de la date d'anniversaire
        const day = new Date(data.birthday).getDate().toString();
        const month =
          new Date(data.birthday).getMonth() + 1 < 10
            ? `0${new Date(data.birthday).getMonth() + 1}`
            : String(new Date(data.birthday).getMonth() + 1);

        const year = new Date(data.birthday).getFullYear().toString();
        const birthday = {
          day,
          month,
          year,
        };

        setState({
          firstname: data.firstname,
          lastname: data.lastname,
          pseudo: data.pseudo,
          num_address: data.numAddress,
          postal_code: data.postalCode,
          address: data.address,
          country: data.country,
          sexe: data.sexe,
          city: data.city,
          birthday,
        });
      } catch (error) {
        HandleError(
          'Erreur',
          error.response.data.error || 'Récupération du profil impossible',
          error.response.status || 500,
        );
        console.log('error: ', error);
      }
    })();
  }, [route]);

  const onSubmit = async () => {
    try {
      const dataToSend = {
        ...state,
        birthday: `${state.birthday.year}-${state.birthday.month}-${state.birthday.day}`,
      };
      const {data} = await axios.put(
        `${API_BASE_URL}/user/${cookie.user.user._id}`,
        dataToSend,
        {
          headers,
        },
      );
      NotifyMessage(data.message);
    } catch (error) {
      console.log('error: ', error);
      // si l'erreur est un tableau d'erreurs des champs
      if (Array.isArray(error.response.data?.error)) {
        //TODO: trouver une solution pour bien afficher les erreurs de modifications qui sont dans un tableau
        HandleError(
          'Erreur',
          error.response.data.error.join(' - '),
          error.response.status || 500,
        );
      } else {
        HandleError(
          'Erreur',
          error.response.data.error || 'Modification du profil impossible',
          error.response.status || 500,
        );
      }
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <Avatar
          containerStyle={{position: 'absolute', top: 25}}
          size="xlarge"
          rounded
          source={require('../../images/pp.png')}>
          <Accessory size={50} />
        </Avatar>
      </View>

      <ScrollView
        style={{
          width: '100%',
          margin: 15,
          marginTop: 100,
        }}>
        <Input
          value={state.firstname}
          placeholder="Prénom"
          onChangeText={(value: string) =>
            setState(prev => ({...prev, firstname: value}))
          }
        />
        <Input
          value={state.lastname}
          placeholder="Nom"
          onChangeText={(value: string) =>
            setState(prev => ({...prev, lastname: value}))
          }
        />
        <Input
          value={state.pseudo}
          placeholder="Pseudo"
          onChangeText={(value: string) =>
            setState(prev => ({...prev, pseudo: value}))
          }
        />

        <Input
          value={state.sexe}
          placeholder="Sexe"
          onChangeText={(value: string) =>
            setState(prev => ({...prev, sexe: value}))
          }
        />
        <View>
          <Text style={{fontSize: 17, marginLeft: 10, marginBottom: 10}}>
            Date de naissance
          </Text>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 2}}>
              <Input
                keyboardType="numeric"
                value={state.birthday.day}
                placeholder="JJ"
                onChangeText={(value: string) =>
                  setState(prev => ({
                    ...prev,
                    birthday: {...prev.birthday, day: value},
                  }))
                }
              />
            </View>
            <View style={{flex: 2}}>
              <Input
                keyboardType="numeric"
                value={state.birthday.month}
                placeholder="MM"
                onChangeText={(value: string) =>
                  setState(prev => ({
                    ...prev,
                    birthday: {...prev.birthday, month: value},
                  }))
                }
              />
            </View>
            <View style={{flex: 2}}>
              <Input
                keyboardType="numeric"
                value={state.birthday.year}
                placeholder="AAAA"
                onChangeText={(value: string) =>
                  setState(prev => ({
                    ...prev,
                    birthday: {...prev.birthday, year: value},
                  }))
                }
              />
            </View>
          </View>
        </View>

        {/* PARTIE ADRESSE */}
        <View style={{marginTop: 50, alignItems: 'center'}}>
          <Text style={{fontSize: 25}}>Adresse</Text>
        </View>
        <Input
          value={state.num_address}
          placeholder="N°"
          onChangeText={(value: string) =>
            setState(prev => ({...prev, num_address: value}))
          }
        />
        <Input
          value={state.address}
          placeholder="adresse"
          onChangeText={(value: string) =>
            setState(prev => ({...prev, address: value}))
          }
        />
        <Input
          value={state.city}
          placeholder="ville"
          onChangeText={(value: string) =>
            setState(prev => ({...prev, city: value}))
          }
        />
        <Input
          value={state.postal_code}
          placeholder="Code postal"
          onChangeText={(value: string) =>
            setState(prev => ({...prev, postal_code: value}))
          }
        />
        <Input
          value={state.country}
          placeholder="Pays"
          onChangeText={(value: string) =>
            setState(prev => ({...prev, country: value}))
          }
        />

        <View style={{alignItems: 'center', marginTop: 15, marginBottom: 15}}>
          <TouchableOpacity onPress={onSubmit} style={styles.button}>
            <Text style={styles.buttonText}>Enregistrer</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  header: {
    height: 100,
    backgroundColor: '#302d42',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    height: 45,
    borderWidth: 0,
    width: '75%',
    backgroundColor: 'white',
    color: 'black',
    borderRadius: 50,
    textAlign: 'center',
    fontSize: 17,
    marginBottom: 20,
  },
  button: {
    height: 45,
    borderWidth: 0,
    width: '75%',
    backgroundColor: '#F8AE6B',
    borderRadius: 50,
    justifyContent: 'center',
  },
  buttonText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  linkBlock: {
    marginTop: -5,
    paddingRight: 5,
    paddingLeft: 5,
    flexDirection: 'row',
    width: '75%',
    justifyContent: 'space-between',
  },
  linkBlockText: {
    color: 'white',
    fontSize: 16,
    textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
  },
});

export default EditProfileScreen;
