/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {SafeAreaView, Text} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import CguComponent from '../components/CguComponent';
const CguScreen = () => {
  return (
    <SafeAreaView style={{backgroundColor: 'white'}}>
      <Text
        style={{
          fontSize: 22,
          fontWeight: 'bold',
          textAlign: 'center',
          color: '#302d42',
          marginBottom: 25,
          marginTop: 25,
        }}>
        Conditions générales d'utilisation
      </Text>
      <ScrollView style={{padding: 15}}>
        <CguComponent />
      </ScrollView>
    </SafeAreaView>
  );
};

export default CguScreen;
