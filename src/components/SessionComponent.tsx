import React from 'react';
import {Image} from 'react-native-elements/dist/image/Image';

interface ISessionComponentProps {
  src: string;
}
const SessionComponent = (props: ISessionComponentProps) => {
  if (!props.src) return null;
  return (
    <Image
      style={{width: '100%', height: '100%'}}
      source={{
        uri: props.src,
      }}
    />
  );
};

export default SessionComponent;
