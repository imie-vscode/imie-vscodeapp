/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet} from 'react-native';
import content from '../utils/CguContent';
import HTMLView from 'react-native-htmlview';
const CguComponent = () => {
  return <HTMLView value={content} stylesheet={styles} />;
};

const styles = StyleSheet.create({
  a: {
    fontWeight: '300',
    color: '#FF3366', // make links coloured pink
  },
  h1: {fontSize: 20},
  h2: {fontSize: 18},
  h3: {fontSize: 16},
});

export default CguComponent;
