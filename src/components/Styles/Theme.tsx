import styled from 'styled-components/native';

interface IContainer {
  paddingBottom?: string | number;
  bg?: string;
  alignItems?: string;
  justifyContent?: string;
}
// contenaire principal
export const Container = styled.View`
  flex: 1;
  padding-bottom: 0;
  background-color: #302d42;
  align-items: flex-start;
  justify-content: flex-start;
`;
