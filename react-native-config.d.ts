declare module 'react-native-config' {
  const GITHUB_CLIENT_ID: string;
  const GITHUB_CLIENT_SECRET: string;

  const GOOGLE_CLIENT_ID: string;

  const SITE_URL: string;
}
